import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import CheckBox from 'react-native-check-box';
export default class AddCreditReportTwo extends Component {
    constructor(props) {
    super(props);

    this.state = {
    listdata:[{isExpanded:false},
    {isExpanded:true},
    {isExpanded:false},
    {isExpanded:true}]
    };
 }

    collapseView(item){
      let rowData = this.state.listdata;
      console.log('before');
       let index = item.index;
      console.log(this.state.listdata);
      rowData[index].isExpanded = !rowData[index].isExpanded;
     
     // console.log(rowData);
      this.setState({
       listdata: rowData
     }, function(){
        console.log('After');
        console.log(this.state.listdata);
     });
    }

     sundayImage(i, event){
    
    const alarmList = this.state.alarmList;
    alarmList[i].sunday = !alarmList[i].sunday;
     Alert.alert(alarmList[i].sunday+' '+i);
   
     this.setState({
       alarmList: alarmList
     });
     
    }

  _renderItem 
  
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
           <View style={{height:'92%', }}>
           <ScrollView >
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Select Packages/Product</Text>
                 <Image style={styles.questionPic} source={require('../image/question.png')}/>
          </View>
              
             <FlatList
                  data={this.state.listdata}
                extraData={this.state}
                keyExtractor={(item, index) => index.toString()}
              renderItem = {(item, index) => (
    <View style={{height: item.item.isExpanded? hp('25%') : hp('19%'),  padding:'4%'}}>
    <View style={{flexDirection:'row', }}>
    <CheckBox
        style={{ paddingStart:'6%', paddingEnd:'4%'}}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
        { item.item.isExpanded ? <View style={{ paddingEnd:'20%', height:hp('15%')}}>
   <Text style={{ lineHeight: 20,}} numberOfLines={5}> hiii ghhcjhxc cdjhkcdnbcdjhksdnxvcjhkcd scjhd cdjhd cjhsd xcjhsfd cxjhfd vffbfuf ief fuf ubef dcfbiefbd ceic xciwfbdcbiewfb</Text>
    </View>
    :
     <View style={{ paddingEnd:'20%', height:hp('9%')}}>
   <Text style={{ lineHeight: 20,}} numberOfLines={2}> hiii ghhcjhxc cdjhkcdnbcdjhksdnxvcjhkcd scjhd cdjhd cjhsd xcjhsfd cxjhfd vffbfuf ief fuf ubef dcfbiefbd ceic xciwfbdcbiewfb</Text>
    </View>}
       
   </View>
   <View style={{ backgroundColor:'gray', height:0.5, marginStart:'5%', marginEnd:'5%'}}></View>
   <View style={{height:hp('6.5%'), paddingEnd:'10%', paddingStart:'10%',
    justifyContent:'space-between', alignItems:'center', flexDirection:'row'}}>
    <TouchableOpacity 
    onPress ={() =>this.collapseView(item)}>
    {/* onPress ={() =>console.log(item)}> */}
    {item.item.isExpanded ?
    <Text style={{fontWeight:'bold', color:'#034d94', fontSize:16}}>Show less</Text>
     :
    <Text style={{fontWeight:'bold', color:'#034d94', fontSize:16}}>Show more</Text>
    }
    </TouchableOpacity>
     <Text style={{fontWeight:'bold',fontSize:20}}>$39</Text>
   </View>
    </View>
     )
  }
              />
            </ScrollView>
            </View>
              <View style={{paddingEnd:'10%', paddingStart:'10%', borderTopWidth:1, borderColor:"#ededed",
    justifyContent:'space-between', alignItems:'center', flexDirection:'row', height:'14%', width:'100%', }}>
    <Text style={{fontWeight:'bold', fontSize:16}}>Total</Text>
     <Text style={{fontWeight:'bold',fontSize:20, color:'#034d94'}}>$39</Text>
    </View>
           </View>
            {/* <View style={styles.footer}>
          <View style={{flexDirection:'row', width:'22%'}}>
          <View style={styles.indicator}></View>
           <View style={styles.indicator}></View>
         </View>
            <TouchableOpacity style={styles.nextBtn}
    //  onPress={() => {this.facebookLogin()}}
       >
       <Text style={styles.nextText}>Reset</Text>
       </TouchableOpacity>
         <View style={styles.signUpBtn}>
        <Text style={{fontSize:13, fontWeight:"bold", color:'white'}}>Save & Make Payment</Text>
        </View>
            </View> */}
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
     paddingBottom:'4%',
  },
  mainView:{
    backgroundColor:'white',
  // flex:1,
   height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
  titleView:{
         height:hp('8%'), 
         justifyContent:'space-between',
          alignItems:'center',
          flexDirection:'row',
          marginStart:'25%', 
          marginEnd:'3%',
         // backgroundColor:'red'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
     borderWidth:2,
     borderColor: "#ededed",
       width:'27%',
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
        signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#008f2c',
     width:'46%',
      marginStart:'4%', 
     marginEnd:'2%',
   borderRadius:10,
    justifyContent:'center',
     alignItems:'center'
     },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              //justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
       questionPic:{
         height:hp('3%'),
          width:hp('3%')
       },

});
