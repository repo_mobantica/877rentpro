
import React, { Component } from 'react'
import { Image,Alert, View, Text, StyleSheet} from 'react-native'
import {Header,Left,Right,Body,Title,Button,Icon} from 'native-base'
import { withNavigation } from 'react-navigation';
import Drawer from 'react-native-drawer';
import ToolbarForFilter from '../component/ToolbarForFilter'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
class Toolbar extends Component {

  render() {
    return (
        <Header style={{backgroundColor:'white'}}>
        <Left>
          <Button transparent onPress={()=>this.props.navigation.openDrawer()}>
            <Icon><Image source={require("../image/menu.png")} style={styles.menuIcon} /></Icon>
          </Button>
        </Left>
        <View style={styles.middleView}>
          {
            this.props.image ? <Image source={require('../image/logo.png')} style={styles.middleImg} /> : <Title style={{textAlign:"center", color:'#034d94'}}>{this.props.title || ""}</Title>
          }
        </View>
        
        <Right>
          <Button transparent onPress={()=>this.props.navigation.navigate('FilterScreen')}>
          <Image source={require('../image/filter.png')} style={styles.filterIcon} />
          </Button>
        </Right>
      </Header>
    )
  }
}

export default withNavigation(Toolbar);

const styles = StyleSheet.create({
middleView:{
  justifyContent:'center',
   alignItems:'center', 
   width:'65%',
    marginRight:'-20%'
},
middleImg:{
  height:50,
  alignSelf:"center", 
},
menuIcon:{
  width:32,height:20 
},
filterIcon:{
  width:25,height:25
}
});