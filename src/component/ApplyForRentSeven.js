import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,TextInput,
Button, FlatList, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';
import DateTimePicker from "react-native-modal-datetime-picker";
export default class ApplyForRentSeven extends Component {

       constructor(props) {
    super(props);

    this.state = {
     firstName:'',
      lastName:'',
      middleName:'',
      address:'',
      birthday:'', 
      socialSecurity:'',
       isDateTimePickerVisible: false,
       occupant:[1],
       referanceContact:[1],
        vehicleDetails:[1],
    };
 }

 showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
     let day =date.getDate();
    let month =date.getMonth();
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     console.log("A date has been picked: ", fullDate);
    this.setState({ Birthday: fullDate });
    this.hideDateTimePicker();
   // Alert.alert(date)
  };
 
    addOccupant(){
      let data = this.state.occupant;
      data.push(1);
      this.setState({
        occupant:data
      });
    }

     addReferanceContact(){
      let data = this.state.referanceContact;
      data.push(1);
      this.setState({
        referanceContact:data
      });
    }
     addVehicleDetails(){
      let data = this.state.vehicleDetails;
      data.push(1);
      this.setState({
        vehicleDetails:data
      });
    }

  _renderItemOccupant = ({item}) => (
    <View style={styles.flatListView}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Proposed Occupants</Text>
        </View>

        <TouchableOpacity 
        style={{height:hp('5%'),marginTop:-hp('2.5%'), marginBottom:-hp('2.5%'), marginStart:'80%', width:hp('5%'), justifyContent:'center', alignItems:'center'}}
       onPress={()=>this.addOccupant()}
       >
         <Image style={styles.addImg} source={require('../image/add.png')}/>
        </TouchableOpacity>
  
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter your name"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Date of Birth</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.birthday === '' ?
             <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
             <Text >{this.state.birthday}</Text> 
             }
             <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={this.showDateTimePicker}>
         <Image style={styles.calenderImg} source={require('../image/calendar.png')}/>
        </TouchableOpacity>
        </View>
        <View style={styles.textinputBorder}></View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Age</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Your age here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>
         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>
         
  
    </View>
  );

    _renderItemRefContact = ({item}) => (
    <View style={[styles.flatListView,{height:hp('30%')}]}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Referance Contact</Text>
        </View>

        <TouchableOpacity 
        style={{height:hp('5%'),  marginTop:-hp('2.5%'), marginBottom:-hp('2.5%'), marginStart:'80%', width:hp('5%'), justifyContent:'center', alignItems:'center'}}
       onPress={()=>this.addReferanceContact()}
       >
         <Image style={styles.addImg} source={require('../image/add.png')}/>
        </TouchableOpacity>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter your name"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>
         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>
         
  
    </View>
  );

  _renderItemVehicle = ({item}) => (
    <View style={styles.flatListView}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Vehicle Details</Text>
        </View>

        <TouchableOpacity 
        style={{height:hp('5%'),  marginTop:-hp('2.5%'), marginBottom:-hp('2.5%'), marginStart:'80%', width:hp('5%'), justifyContent:'center', alignItems:'center'}}
       onPress={()=>this.addVehicleDetails()}
       >
         <Image style={styles.addImg} source={require('../image/add.png')}/>
        </TouchableOpacity>
  
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Phone</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Phone number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>

        

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Phone</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Phone number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Phone</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Phone number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Phone</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Phone number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>
         
  
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
      <View style={styles.container}>
      <ToolbarWithLeftIcon title="My Tenants" />
             <ScrollView>
       <FlatList
        data={this.state.occupant}
        extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItemOccupant}
      />
   <View style={{height:hp('1%'), width:'100%'}}></View>

   <FlatList
        data={this.state.referanceContact}
        extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItemRefContact}
      />
   <View style={{height:hp('1%'), width:'100%'}}></View>

   <View style={[styles.flatListView,{height:hp('30%')}]}>
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Referance Supervisor Contact</Text>
        </View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Name here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>
         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
         <View style={styles.textinputBorder}></View>
       </View>
         <View style={{height:hp('1%'), width:'100%'}}></View>

         <FlatList
        data={this.state.vehicleDetails}
        extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItemVehicle}
      />
   <View style={{height:hp('1%'), width:'100%'}}></View>
     </ScrollView>
          </View>

           <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
   flatListView:{
     height:hp('52%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
   nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
  calenderImg:{
         height:hp('3%'),
       width:hp('3%')
      },
       addImg:{
         height:hp('5%'),
       width:hp('5%')
      },
  nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
      marginEnd:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
       uploadView:{
         height:hp('8%'),
          marginStart:'8%',
           marginEnd:'8%',
           marginTop:hp('1%'),
            borderRadius:10,
             borderStyle: 'dotted',
             borderWidth: 1,
              borderColor:'gray',
               flex:1,
                flexDirection:'row', 
                alignItems:'center', 
                justifyContent:'space-around'
          },
      calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
       },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
});