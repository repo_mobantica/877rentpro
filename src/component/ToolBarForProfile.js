import React, { Component } from 'react'
import { Image,Alert, View } from 'react-native'
import {Header,Left,Right,Body,Title,Button,Icon} from 'native-base'
import { withNavigation } from 'react-navigation';
import Drawer from 'react-native-drawer';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
class ToolBarForProfile extends Component {

  render() {
    return (
        <Header style={{backgroundColor:'white'}}>
        <Left>
          <Button transparent onPress={()=>this.props.navigation.openDrawer()}>
            <Icon><Image source={require("../image/left.png")} style={{width:25,height:20 }} /></Icon>
          </Button>
        </Left>
        <View style={{justifyContent:'center', alignItems:'center', width:'65%',  marginEnd:'-20%'}}>
             <Title style={{textAlign:"center", color:'#034d94'}}>{this.props.title || ""}</Title>
        </View>
        
        <Right>
          <Button transparent onPress={()=>this.props.navigation.openDrawer()}>
          <Image source={require('../image/edit.png')} style={{width:25,height:21}} />
          </Button>
        </Right>
      </Header>
    )
  }
}

export default withNavigation(ToolBarForProfile);