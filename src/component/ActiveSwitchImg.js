 import React, { Component } from 'react';
import { Text, View, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export default class ActiveSwitchImg extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
       <Image source ={require('../image/pets_active.png')}
       style={{height:11,
                width: 11,
     }}/>
      
      </View>
    );
  }
}



// import React from 'react';
// import { StyleSheet, Text, View, Alert, Image, Button } from 'react-native';
// import {
//   GoogleSignin,
//   GoogleSigninButton,
//   statusCodes,
// } from 'react-native-google-signin';
// import {
//   LoginButton,
//   AccessToken,
//   GraphRequest,
//   GraphRequestManager,
// } from 'react-native-fbsdk';
//  import AsyncStorage from '@react-native-community/async-storage';
//  import SideBarDrawer from './SideBarDrawer';
// export default class Login extends React.Component {
//   constructor(props) {
//     super(props);
//   //   Obj = new SideBarDrawer();
//     this.state = {
//       userInfo: '',
//       user_name: '',
//       token: '',
//       profile_pic: '',
//     };
//   }
//   componentDidMount() {
//     GoogleSignin.configure({
//       //It is mandatory to call this method before attempting to call signIn()
//       scopes: ['https://www.googleapis.com/auth/drive.readonly'],
//       // Repleace with your webClientId generated from Firebase console
    
//         webClientId: '323867318004-vmgm4t4crc2i1sn7cknh80tkj2rsnc56.apps.googleusercontent.com',
//     });
//   }
//   _signIn = async () => {
//     //Prompts a modal to let the user sign in into your application.
//     try {
//        console.log('step 1');
//       await GoogleSignin.hasPlayServices({
//         //Check if device has Google Play Services installed.
//         //Always resolves to true on iOS.
        
//         showPlayServicesUpdateDialog: true,
        
//       });
//        console.log('step 3');
//       const userInfo = await GoogleSignin.signIn();
//       console.log('User Info --> ', userInfo);
//       this.setState({ userInfo: userInfo });
//     } catch (error) {
//       console.log('Message', error.message);
//       if (error.code === statusCodes.SIGN_IN_CANCELLED) {
//         console.log('User Cancelled the Login Flow');
//       } else if (error.code === statusCodes.IN_PROGRESS) {
//         console.log('Signing In');
//       } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
//         console.log('Play Services Not Available or Outdated');
//       } else {
//         console.log('Some Other Error Happened');
//       }
//     }
//   };
//   _getCurrentUser = async () => {
//     //May be called eg. in the componentDidMount of your main component.
//     //This method returns the current user
//     //if they already signed in and null otherwise.
//     try {
//       const userInfo = await GoogleSignin.signInSilently();
//       this.setState({ userInfo });
//     } catch (error) {
//       console.error(error);
//     }
//   };
//   _signOut = async () => {
//     //Remove user session from the device.
//     try {
//       await GoogleSignin.revokeAccess();
//       await GoogleSignin.signOut();
//       this.setState({ user: null }); // Remove the user from your app's state as well
//     } catch (error) {
//       console.error(error);
//     }
//   };
//   _revokeAccess = async () => {
//     //Remove your application from the user authorized applications.
//     try {
//       await GoogleSignin.revokeAccess();
//       console.log('deleted');
//     } catch (error) {
//       console.error(error);
//     }
//   };

// get_Response_Info = async(error, result) => {
//     if (error) {
//       //Alert for the Error
//       Alert.alert('Error fetching data: ' + error.toString());
//     } else {
//       //response alert
//     //  alert(JSON.stringify(result));
//       // this.setState({ user_name: 'Welcome' + ' ' + result.name });
//       // this.setState({ token: 'User Token: ' + ' ' + result.id });
//       // this.setState({ profile_pic: result.picture.data.url });
//        await AsyncStorage.setItem('userName', result.name);
//         const value = await AsyncStorage.getItem('userName');
//        // Obj.retrieveData();
//        console.log('11111');
//        console.log(value);
//         console.log('11111');
//     }
//   };
 
 
//   onLogout = async() => {
//     //Clear the state after logout
//     //this.setState({ user_name: null, token: null, profile_pic: null });
//      await AsyncStorage.setItem('userName', '');
//   //   Obj.retrieveData();
//   };
 
//   render() {
//     return (
//       <View style={styles.container}>
//       {this.state.profile_pic ? (
//           <Image
//             source={{ uri: this.state.profile_pic }}
//             style={styles.imageStyle}
//           />
//         ) : null}
//         <Text style={styles.text}> {this.state.user_name} </Text>
//         <Text> {this.state.token} </Text>
//         <LoginButton
//           readPermissions={['public_profile']}
//           onLoginFinished={(error, result) => {
//             if (error) {
//               alert(error);
//               alert('login has error: ' + result.error);
//             } else if (result.isCancelled) {
//               alert('login is cancelled.');
//             } else {
//               AccessToken.getCurrentAccessToken().then(data => {
//               //  alert(data.accessToken.toString());
 
//                 const processRequest = new GraphRequest(
//                   '/me?fields=name,picture.type(large)',
//                   null,
//                   this.get_Response_Info
//                 );
//                 // Start the graph request.
//                 new GraphRequestManager().addRequest(processRequest).start();
//               });
//             }
//           }}
//           onLogoutFinished={this.onLogout}
//         />
        
//         <GoogleSigninButton
//           style={{ width: 240, height: 40, marginTop:10 }}
//           size={GoogleSigninButton.Size.Wide}
//           color={GoogleSigninButton.Color.Light}
//           onPress={this._signIn}
//         />
         
//       </View>
//     );
//   }
// }
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//    text: {
//     fontSize: 20,
//     color: '#000',
//     textAlign: 'center',
//     padding: 20,
//   },
//   imageStyle: {
//     width: 200,
//     height: 300,
//     resizeMode: 'contain',
//   },
// });