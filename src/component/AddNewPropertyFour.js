import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import RNPickerSelect from 'react-native-picker-select';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: 'param1', value: 0 },
  {label: 'param2', value: 1 }
];

export default class AddNewPropertyFour extends Component {
    constructor(props) {
    super(props);

    this.state = {
     firstName:'',
      lastName:'',
      middleName:'',
      address:'',
      birthday:'', 
      socialSecurity:'',
       isDateTimePickerVisible: false,
    };
 }

 showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
     let day =date.getDate();
    let month =date.getMonth();
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     console.log("A date has been picked: ", fullDate);
    this.setState({ Birthday: fullDate });
    this.hideDateTimePicker();
   // Alert.alert(date)
  };
 
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <View style={styles.mainView}>
              <ScrollView>
            <Text style={styles.nameStyle}>Availability</Text>
            <View style={{height:hp('7%'), justifyContent:'center', paddingStart:wp('10%') }}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({value:value})}}
                   />
             </View>

               <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Birthday</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.birthday === '' ?
             <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
             <Text >{this.state.birthday}</Text> 
             }
             <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={this.showDateTimePicker}>
         <Image style={styles.calenderImg} source={require('../image/calendar.png')}/>
        </TouchableOpacity>
        </View>
        <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>Middle Name</Text>
         <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
                { label: 'Football', value: 'football' },
                { label: 'Baseball', value: 'baseball' },
                { label: 'Hockey', value: 'hockey' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>

        <Text style={styles.nameStyle}>Photo Gallery</Text>
         <View style={styles.uploadView}>
    <Text style={{color:'gray'}}>Drop/Upload file here</Text>
     <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
    </View> 

    <Text style={styles.nameStyle}>Primary photo</Text>
         <View style={styles.uploadView}>
    <Text style={{color:'gray'}}>Drop/Upload file here</Text>
     <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
    </View> 

    <Text style={styles.nameStyle}>Middle Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(middleName) => this.setState({middleName})}
          value={this.state.middleName}
        />
        <View style={styles.textinputBorder}></View>
           </ScrollView>
           </View>
           
              <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
    paddingBottom:'4%',
  },
  mainView:{
    backgroundColor:'white',
  // flex:1,
   height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
  calenderImg:{
         height:hp('3%'),
       width:hp('3%')
      },
  nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
      marginEnd:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
       uploadView:{
         height:hp('8%'),
          marginStart:'8%',
           marginEnd:'8%',
           marginTop:hp('1%'),
            borderRadius:10,
             borderStyle: 'dotted',
             borderWidth: 1,
              borderColor:'gray',
               flex:1,
                flexDirection:'row', 
                alignItems:'center', 
                justifyContent:'space-around'
          },
      calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
       },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       }
});
