import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import CheckBox from 'react-native-check-box';
import Dash from 'react-native-dash';
import RNPickerSelect from 'react-native-picker-select';

export default class ApplyForRentSix extends Component {
    constructor(props) {
    super(props);

    this.state = {
     checked:0,
     data:['Student', 'Employed', 'Self Employed  '],
      catCheck:false,
      dogCheck:false,
      occupation:'',
      income:'',
      frequency:'',
      otherSource:''
    };
 }


  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Employment Details</Text>
            </View>

            <Text style={styles.nameStyle}>I am a,</Text>

              <View style={styles.radioBtnRow}>
             <TouchableOpacity style= {styles.radioBtn} onPress={() =>{this.setState({checked:0})}}>
                {
                   this.state.checked == 0 ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[0]}</Text>
                </TouchableOpacity>

                 <TouchableOpacity style= {styles.radioBtn}   onPress={() =>{this.setState({checked:1})}}>
                {
                   this.state.checked == 1 ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[1]}</Text>
                </TouchableOpacity>
                </View>
                 <View style={styles.radioBtnRow}>
                <TouchableOpacity style= {styles.radioBtn}  onPress={() =>{this.setState({checked:2})}}>
                {
                   this.state.checked == 2 ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[2]}</Text>
                </TouchableOpacity>
                </View>
           
                   <Text style={styles.nameStyle}>Pets allowed?</Text>
                   <View style={styles.radioBtnRow}>
                 <CheckBox
                   style={styles.checkBoxStyle}
                     checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
                     onClick={()=>{
                     this.setState({
                      catCheck:!this.state.catCheck
                      })
                       }}
                   isChecked={this.state.catCheck}
                   />
                    <Text style={styles.checkBoxText}>Cat</Text>

                    <CheckBox
                   style={styles.checkBoxStyle}
                     checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
                     onClick={()=>{
                     this.setState({
                      dogCheck:!this.state.dogCheck
                      })
                       }}
                   isChecked={this.state.dogCheck}
                   />
                    <Text style={styles.checkBoxText}>Dog</Text>
            </View>

             <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Present Occupation</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Present Occupation here"
          onChangeText={(occupation) => this.setState({occupation})}
          value={this.state.occupation}
        />
         <View style={styles.textinputBorder}></View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Current Gross Income</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Current Gross Income"
          onChangeText={(income) => this.setState({income})}
          value={this.state.income}
        />
         <View style={styles.textinputBorder}></View>

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Frequency </Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
                { label: 'Football', value: 'football' },
                { label: 'Baseball', value: 'baseball' },
                { label: 'Hockey', value: 'hockey' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>

 <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Other Source</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Other Source here"
          onChangeText={(otherSource) => this.setState({otherSource})}
          value={this.state.otherSource}
        />
         <View style={styles.textinputBorder}></View>

            </ScrollView>       
           </View>  
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     radioImg:{
       height:hp('4%'),
       width:hp('4%'),
      marginRight:10
    },
     radioBtn:{
          flexDirection:'row',
     alignItems:'center',
     width:'40%',
     // backgroundColor:'red'
 },
 radioBtnRow:{
   flexDirection:'row',
    height:hp('7%'),
     alignItems:'center', 
     paddingStart:'10%',
     paddingEnd:'10%'
},
 nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
     fontSize:16,
     color:'gray'
  // backgroundColor:'yellow', 
 }, 
  checkBoxStyle:{ 
        paddingEnd: 10
  },
  checkBoxText:{
    width:'40%' 
    },
});
