import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import CheckBox from 'react-native-check-box';
export default class AddNewPropertyOne extends Component {
    constructor(props) {
    super(props);
    this.state = {
     
    };
 }
    
    _renderItem = ({item}) => {
      console.log(typeof item);
      //let a = item;
      return(  
     <View style={styles.flatListView}>
      <View style={styles.listTitle}>
      <Text>Parking</Text>
      <Image
          style={styles.iconStyle}
          source={require('../image/droparrow.png')}
       />
      </View>

     <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Pool</Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Exercise Facility</Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Furnished</Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Un Furnished</Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Elevator</Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Parking </Text>
      </View>
       </View>

        <View style={styles.listRow}>
    <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Pool</Text>
      </View>
      <View style={styles.checkboxView}>
       <CheckBox
        style={styles.checkboxStyle}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
         <Text>Ameneties</Text>
      </View>
       </View>

    </View>
  );
   
   }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <FlatList
        data={[1,2,3,4]}
       extraData={this.state}
         keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
          
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'2%'
  },
   flatListView:{
     height:hp('29%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
   listTitle:{
     height:hp('7%'),
      flexDirection:'row',
       marginStart:'5%',
    marginEnd:'5%',
     borderBottomWidth:0.5, 
      justifyContent:'space-between', 
       alignItems:'center',
  },
  iconStyle:{
    height:hp('4%'),
     width:hp('4%')
  },
   listRow:{
     height:hp('5.5%'),
      flexDirection:'row',
       marginStart:'5%',
        marginEnd:'5%', 
         alignItems:'center',
    },
    checkboxView:{
       flexDirection:'row', 
        alignItems:'center',
         width:'50%', 
  }, 
  checkboxStyle:{ 
     paddingEnd:'4%'
     },
});