import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: 'param1', value: 0 },
  {label: 'param2', value: 1 }
];

export default class ApplyForRentEight extends Component {
    constructor(props) {
    super(props);

    this.state = {
        isExpanded:false
      };
    }
   collapseView(){
      this.setState({
       isExpanded: !this.state.isExpanded
     });
    }

  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
            <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>Important and Disclaimer</Text>
            </View>

               <Text style={styles.nameStyle}>Have you ever been evicted/asked to move?</Text>
                 <View style={styles.radioBtn}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({value:value})}}
                   />
             </View>

              <Text style={styles.nameStyle}>Have you ever filed bankrucpy?</Text>
                <View style={styles.radioBtn}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({value:value})}}
                   />
             </View>

              <Text style={styles.nameStyle}>Have you ever been convicted of selling distributing or manufacturing illegal drugs?</Text>
                <View style={styles.radioBtn}>
              <RadioForm
               radio_props={radio_props}
              // initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({value:value})}}
                   />
             </View>

              <Text style={styles.uploadTextStyle}>Uplod Id</Text>
               <View style={styles.uploadView}>
               <Text style={{color:'gray'}}>Drop/Upload file here</Text>
                   <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
              </View>

              <Text style={styles.uploadTextStyle}>Income Proof</Text>
               <View style={styles.uploadView}>
               <Text style={{color:'gray'}}>Drop/Upload file here</Text>
                   <Image style={styles.calenderImg} source={require('../image/upload.png')}/>
             </View>

             <Text style={styles.uploadTextStyle}>Sign Here</Text>
               <View style={styles.signatureView}></View>

                 <Text style={styles.uploadTextStyle}>Disclaimer</Text>

                  { this.state.isExpanded ?
                   <View style={styles.disclaimerView}>
               <Text style={styles.disclaimerText} > hiii ghhcjhxc cdjhkcdnbfvvvgjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
             jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
             jjjjjjjjjjjjjjjjjjjjjjjjjjcdjhksdnxvcjhkcd scjhd cdjhd cjhsd xcjhsfd cxjhfd vffbfuf ief fuf ubef dcfbiefbd ceic xciwfbdcbiewfb</Text>
               <TouchableOpacity 
                onPress ={() =>this.collapseView()}>
                <Text style={styles.moreOrLess}>Read less</Text>
               </TouchableOpacity>
               </View>
               :
                <View style={[styles.disclaimerView,{height:hp('20%')}]}>
             <Text style={styles.disclaimerText} numberOfLines={5}> hiii ghhcjhxc cdjhkcdnbfvvvgjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
             jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
             jjjjjjjjjjjjjjjjjjjjjjjjjjcdjhksdnxvcjhkcd scjhd cdjhd cjhsd xcjhsfd cxjhfd vffbfuf ief fuf ubef dcfbiefbd ceic xciwfbdcbiewfb</Text>
              <TouchableOpacity 
               onPress ={() =>this.collapseView()}>
              <Text style={styles.moreOrLess}>Read more</Text>
            </TouchableOpacity>
          </View>}
           </ScrollView>
           </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
      nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
     marginEnd:wp('10%'),
     fontSize:14,
     color:'gray'
  // backgroundColor:'yellow', 
 },  
 uploadTextStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
   uploadView:{
         height:hp('8%'),
          marginStart:'8%',
           marginEnd:'8%',
           marginTop:hp('1%'),
            borderRadius:10,
             borderStyle: 'dotted',
             borderWidth: 1,
              borderColor:'gray',
               flex:1,
                flexDirection:'row', 
                alignItems:'center', 
                justifyContent:'space-around'
 },
   calenderImg:{
         height:hp('3%'),
       width:hp('3%')
  },
  signatureView:{
    marginStart:'10%', 
    marginEnd:'10%',
   borderRadius:10,
  borderWidth:1,
   height:hp('16%'),
  marginTop:hp('2%'),
   marginBottom:hp('2%'),
   backgroundColor:'#d3e6ef',
   borderColor:'#034d94'
  },
  disclaimerView:{
     paddingEnd:'10%',
     // height:hp('30%'),
       paddingStart:'10%',
       flex:1,
        marginTop:hp('2%')
  },
  disclaimerText:{
     lineHeight: 20, 
     color:'#737373',
  },
  moreOrLess:{
    fontWeight:'bold',
     color:'#034d94', 
    fontSize:16, 
    textAlign:'right'
  },
  radioBtn:{
    height:hp('7%'),
     justifyContent:'center',
      paddingStart:wp('10%') 
  }
});
