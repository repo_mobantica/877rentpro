import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import CheckBox from 'react-native-check-box'
export default class AddNewPropertyOne extends Component {
    constructor(props) {
    super(props);

    this.state = {
     address:'',
     displayAddress:'',
     unitDetails:''
    };
 }

 showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
     let day =date.getDate();
    let month =date.getMonth();
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     console.log("A date has been picked: ", fullDate);
    this.setState({ birthday: fullDate });
    this.hideDateTimePicker();
   // Alert.alert(date)
  };
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
            <View style={styles.titleView}>
            <Text style={styles.titleText}>Present Address</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Actual Address</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Address here"
          onChangeText={(address) => this.setState({address})}
          value={this.state.address}
          multiline={true}
        />
        </View>
        <View style={styles.textinputBorder}></View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Display Address</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
           style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Same Address appear here"
          onChangeText={(displayAddress) => this.setState({displayAddress})}
          value={this.state.displayAddress}
          multiline={true}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        <View style={styles.searchBox}>
       <CheckBox
        style={{ paddingEnd:'2%'}}
         onClick={()=>{
         this.setState({
             isSingleFam:!this.state.isSingleFam
          })
        }}
       isChecked={this.state.isSingleFam}
        />
        <Text>Same as Actual Address</Text>
        </View>
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Unit Details</Text>
             </View>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="unit details"
          onChangeText={(unitDetails) => this.setState({unitDetails})}
          value={this.state.unitDetails}
          multiline={true}
        />
        </View>
        <View style={styles.textinputBorder}></View>
           
           </View>
            {/* <View style={styles.footer}>
          <View style={{flexDirection:'row'}}>
          <View style={styles.indicator}></View>
          <View style={[styles.indicator,{backgroundColor:'white', borderWidth:0.5}]}></View>
         </View>
            <TouchableOpacity style={styles.nextBtn}
    //  onPress={() => {this.facebookLogin()}}
       >
       <Text style={styles.nextText}>Next</Text>
       </TouchableOpacity>
            </View> */}
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
       indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
       textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
        },
        searchBox:{
            height:hp('5%'),
             flexDirection:'row',
              alignItems:'flex-start',
               paddingStart:'10%'
        },
});
