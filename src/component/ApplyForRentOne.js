import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import CheckBox from 'react-native-check-box';
import DateTimePicker from "react-native-modal-datetime-picker";
export default class ApplyForRentOne extends Component {
    constructor(props) {
    super(props);

    this.state = {
      firstName:'',
      lastName:'',
      email:'',
      birthday:'',
      income:'',
      mobileNo:'',
      workPhone:'',
      isDateTimePickerVisible: false,
       value: 0,
    };
 }

showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
     let day =date.getDate();
    let month =date.getMonth();
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     console.log("A date has been picked: ", fullDate);
    this.setState({ Birthday: fullDate });
    this.hideDateTimePicker();
   // Alert.alert(date)
  };
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
           <View style={styles.mainView}>
           <ScrollView>
             <View style={styles.titleView}>
            <Text style={styles.titleText}>General Account Information</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>First Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter First Name"
          onChangeText={(firstName) => this.setState({firstName})}
          value={this.state.firstName}
        />
         <View style={styles.textinputBorder}></View>

          <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Last Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Last Name"
          onChangeText={(lastName) => this.setState({lastName})}
          value={this.state.lastName}
        />
         <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>Email</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Email"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />
        <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Date of Birth</Text>
             </View>
             <View style={styles.birthdayView}>
             {this.state.birthday === '' ?
             <Text style={{color:'gray'}}>DD/MM/YYYY</Text> :
             <Text >{this.state.birthday}</Text> 
             }
             <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={this.showDateTimePicker}>
         <Image style={styles.calenderImg} source={require('../image/calendar.png')}/>
        </TouchableOpacity>
        </View>
        <View style={styles.textinputBorder}></View>
           
            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Mobile Number"
          onChangeText={(mobileNo) => this.setState({mobileNo})}
          value={this.state.mobileNo}
        />
         <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>Work Phone Number</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Work Phone Number"
          onChangeText={(workPhone) => this.setState({workPhonesss})}
          value={this.state.workPhone}
        />
        <View style={styles.textinputBorder}></View>

         <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             secondCheck:!this.state.secondCheck
          })
        }}
       isChecked={this.state.secondCheck}
        />
        <Text style={{color:'gray'}}>Guarantor</Text>
        </View>
         <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
           </ScrollView>
           </View>
           
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
   paddingBottom:'3%'
  },
  mainView:{
    backgroundColor:'white',
     height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'), 
      marginBottom:hp('1.2%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
        calenderImg:{
         height:hp('3%'),
       width:hp('3%')
      },  
      nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
  birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
   },
    checkBoxView:{
            height:hp('5%'),
             flexDirection:'row',
              marginStart:'10%', 
              alignItems:'center', 
    },
    checkBoxStyle:{ 
        paddingEnd: 10
  },
});
