import React, { Component } from 'react'
import { Image,Alert, View, Text, StyleSheet} from 'react-native'
import {Header,Left,Right,Body,Title,Button,Icon} from 'native-base'
import { withNavigation } from 'react-navigation';
import Drawer from 'react-native-drawer';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
class ToolbarForFilter extends Component {

  render() {
    return (
        <Header style={{backgroundColor:'white'}}>
        <Left>
          <Button transparent onPress={()=>this.props.navigation.openDrawer()}>
            <Icon><Image source={require("../image/menu.png")} style={styles.menuIcon} /></Icon>
          </Button>
        </Left>
        <View style={styles.middleView}>
                  <Text style={styles.middletext}> Filter</Text>
        </View>
        
        <Right>
          <Button transparent onPress={()=>this.props.navigation.navigate('FilterScreen')}>
          <Text style={{color:'gray'}}> Clear all</Text>
          </Button>
        </Right>
      </Header>
    )
  }
}

export default withNavigation(ToolbarForFilter);
const styles = StyleSheet.create({
middleView:{
  justifyContent:'center',
   alignItems:'center',
    width:'65%',
     marginRight:'-20%'
},
middletext:{
  color:'#034d94',
   fontSize:16,
    fontWeight:'bold'
},
menuIcon:{
  width:32,height:20 
}  
});