import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import DateTimePicker from "react-native-modal-datetime-picker";
import Dash from 'react-native-dash';
import RNPickerSelect from 'react-native-picker-select';
import CheckBox from 'react-native-check-box'

export default class AddNewPropertyFive extends Component {
    constructor(props) {
    super(props);

    this.state = {
     firstName:'',
      lastName:'',
      mobile:'',
      email:'',
      firstCheck:true,
      secondCheck:true,
      thirdCheck:true,
      data: ['Free', 'Gold', 'Platinum', 'Diamond'],
    checked:0
    };
 }
 
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
         
           <View style={styles.mainView}>
              <ScrollView>
              <Text style={styles.nameStyle}>First Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(firstName) => this.setState({firstName})}
          value={this.state.firstName}
        />
        <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>last Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(lastName) => this.setState({lastName})}
          value={this.state.lastName}
        />
        <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>Email</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />
        <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>Mobile</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(mobile) => this.setState({mobile})}
          value={this.state.mobile}
        />
        <View style={styles.textinputBorder}></View>

        <Text style={styles.nameStyle}>Select Package</Text>

              <View style={styles.radioBtnRow}>
             <TouchableOpacity style= {styles.radioBtn} onPress={() =>{this.setState({checked:0})}}>
                {
                   this.state.checked == 0 ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[0]}</Text>
                </TouchableOpacity>

                 <TouchableOpacity style= {styles.radioBtn}   onPress={() =>{this.setState({checked:1})}}>
                {
                   this.state.checked == 1 ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[1]}</Text>
                </TouchableOpacity>
                </View>
                 <View style={styles.radioBtnRow}>
                <TouchableOpacity style= {styles.radioBtn}  onPress={() =>{this.setState({checked:2})}}>
                {
                   this.state.checked == 2 ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[2]}</Text>
                </TouchableOpacity>

                <TouchableOpacity style= {styles.radioBtn}  onPress={() =>{this.setState({checked:3})}}>
                {
                   this.state.checked == 3 ?
                    <Image style={styles.radioImg} source={require('../image/blueCheck.png')}/>
                    :
                    <Image style={styles.radioImg} source={require('../image/UnCheckedBlue.png')}/>
                }
                 <Text>{this.state.data[3]}</Text>
                </TouchableOpacity>
                 </View>

         <View style={styles.signUpBtn}>
          <Image
              style={{height:hp('4%'), width:hp('4%')}}
              source={require("../image/filter.png")}
            />
        <Text style={styles.packageDetailText}>View Package Details</Text>
        </View>

         <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             firstCheck:!this.state.firstCheck
          })
        }}
       isChecked={this.state.firstCheck}
        />
        <Text style={{color:'gray'}}>Rent Collect Add On</Text>
        </View>

         <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             secondCheck:!this.state.secondCheck
          })
        }}
       isChecked={this.state.secondCheck}
        />
        <Text style={{color:'gray'}}>Hide Property</Text>
        </View>

        <View style={styles.checkBoxView}>
        <CheckBox
        style={styles.checkBoxStyle}
        checkBoxColor={'#034d94'}
        checkedCheckBoxColor={'#034d94'}
         onClick={()=>{
         this.setState({
             thirdCheck:!this.state.thirdCheck
          })
        }}
       isChecked={this.state.thirdCheck}
        />
        <Text style={{color:'gray'}}>I Accept All Term and Condition</Text>
        </View>
           </ScrollView>
           </View>
           
             
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed",
    paddingBottom:'4%',
  },
  mainView:{
    backgroundColor:'white',
  // flex:1,
   height:'100%',
   margin:'5%', 
  borderRadius:10,
  },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
  nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
      marginEnd:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
          signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#034d94',
     width:'70%',
      marginStart:'18%', 
     marginEnd:'18%',
  marginTop:hp('2%'),
   marginBottom:hp('4%'), 
   borderRadius:10,
   flexDirection:'row',
    justifyContent:'space-between',
     alignItems:'center',
     paddingEnd:'3%',
     paddingStart:'3%'
     },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
        checkBoxView:{
            height:hp('5%'),
             flexDirection:'row',
              marginStart:'10%', 
              alignItems:'center', 
    },
    checkBoxStyle:{ 
        paddingEnd: 10
  },
  packageDetailText:{
      fontSize:14, 
      fontWeight:"bold",
       color:'white'
    },
    radioImg:{
       height:hp('4%'),
       width:hp('4%'),
      marginRight:10
    },
     radioBtn:{
          flexDirection:'row',
     alignItems:'center',
     width:'40%',
     // backgroundColor:'red'
 },
 radioBtnRow:{
   flexDirection:'row',
    height:hp('7%'),
     alignItems:'center', 
     paddingStart:'10%'
}
});