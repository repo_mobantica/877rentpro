
import {Dimensions,Platform,PixelRatio} from "react-native";
const {width,height} = Dimensions.get("window");

export const config = {
    primaryColor:"#2e87e3",
    secondaryColor:"#01a707",
    infoColor:"#ef5e25",
    width:width,
    height:height,
    platform:Platform.OS
}