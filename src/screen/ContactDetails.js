import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarWithNavigation from '../component/ToolbarWithNavigation';
export default class ContactDetails extends Component {
    constructor(props) {
    super(props);

    this.state = {
      password:'',
      newPass:'',
      confirmPass:''
    };   
  }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
             <ToolbarWithNavigation title="Add New Property" />
           <View style={styles.mainView}>

        <View style={[styles.floatingView,{height:'29%',}]}>
           <View style={styles.imageView}>
                <Image style={styles.flotingImage} source={require('../image/MapPointerSelected.png')}/>
                 </View>
                 <Text style={styles.addressTitle}>Address</Text>

         <Text numberOfLines={3} style={styles.addressText}>Rentpro Property
                    3300 Durango Ave Los Angeles
                      California 90034</Text>
         </View>

           <View style={styles.floatingView}>
           <View style={styles.imageView}>
                <Image style={styles.flotingImage} source={require('../image/PhoneBlue.png')}/>
                 </View>
                 <Text style={styles.addressTitle}>Let's Talk</Text>

         <Text numberOfLines={1} style={styles.addressText}>877-745-4567</Text>
         </View>

           <View style={styles.floatingView}>
           <View style={styles.imageView}>
                <Image style={styles.flotingImage} source={require('../image/EmailBlue.png')}/>
                 </View>
                 <Text style={styles.addressTitle}>Email Us</Text>

         <Text numberOfLines={3} style={styles.addressText}>info@877Rentpro.com</Text>
         </View>
               

        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:hp('85%'),
     width:'90%',
     margin:wp('5%'), 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1
},
 flotingImage:{
        height:hp('4%'),
         width:hp('4%'),
 },
floatingView:{
  height:'23%',
   width:'100%', 
 // justifyContent: "center",
  alignItems:'center' ,
  marginTop:'8%', 
 // backgroundColor:'red',
 
},
  imageView:{
 backgroundColor:'#f3f3f3', 
 //marginTop:'10%',
 alignItems:'center',
 justifyContent:'center',
 borderRadius:hp('3.5%'), 
 height:hp('7%'),
 width:hp('7%')
 },
 addressTitle:{
   margin:'3%',
    fontSize:16,
     fontWeight:'bold', 
     color:'#034d94'
 },
 addressText:{
   fontSize:16, 
   marginStart:'15%',
    marginEnd:'15%',
 }
});

//  <View style={styles.mainView}>

//         <View style={styles.iconRow}>
//          <Image
//               style={styles.icon}
//               source={require("../image/home.png")}
//             />
//             <Text style={styles.subTitle}>Address</Text>
//         </View>
//         <View style={[styles.textStyle,{height:hp('7%')}]}>
//         <Text numberOfLines={3}>Rentpro Property
//                     3300 Durango Ave Los Angeles
//                      California 90034</Text>
//         </View>
        
//          <View style={styles.iconRow}>
//          <Image
//               style={styles.icon}
//               source={require("../image/call.png")}
//             />
//             <Text style={styles.subTitle}>Lets Talk</Text>
//         </View>
//         <View style={styles.textStyle}>
//         <Text numberOfLines={1}>Info: 877-745-4567</Text>
//         </View>
//          <View style={styles.iconRow}>
//          <Image
//               style={styles.icon}
//               source={require("../image/email.png")}
//             />
//             <Text style={styles.subTitle}>Send us an Email</Text>
//         </View>
//         <View style={styles.textStyle}>
//         <Text numberOfLines={1}>info: info@877Rentpro.com</Text>
//         </View>

//         </View>