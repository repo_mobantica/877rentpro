// import React, { Component } from 'react';
// import { View, StyleSheet, Text, Alert, Image, Button } from 'react-native';
// import {
//   LoginButton,
//   AccessToken,
//   GraphRequest,
//   GraphRequestManager,
// } from 'react-native-fbsdk';
//  import AsyncStorage from '@react-native-community/async-storage';
// export default class Login extends Component {
//   constructor() {
//     super();
//     //Setting the state for the data after login
//     this.state = {
//       user_name: '',
//       token: '',
//       profile_pic: '',
//     };
//   }
 
//   get_Response_Info = async(error, result) => {
//     if (error) {
//       //Alert for the Error
//       Alert.alert('Error fetching data: ' + error.toString());
//     } else {
//       //response alert
//     //  alert(JSON.stringify(result));
//       this.setState({ user_name: 'Welcome' + ' ' + result.name });
//       this.setState({ token: 'User Token: ' + ' ' + result.id });
//       this.setState({ profile_pic: result.picture.data.url });
//        await AsyncStorage.setItem('userName', result.name);
//         const value = await AsyncStorage.getItem('userName');
//     //    console.log('value');
//     //    console.log(value);
//     //     console.log('value');
//     }
//   };
 
 
//   onLogout = () => {
//     //Clear the state after logout
//     this.setState({ user_name: null, token: null, profile_pic: null });
//   };
//    onLogout11 = () => {
//     //Clear the state after logout
//     this.setState({ user_name: null, token: null, profile_pic: null });
//   };
 
//   render() {
//     return (
//       <View style={styles.container}>
//         {this.state.profile_pic ? (
//           <Image
//             source={{ uri: this.state.profile_pic }}
//             style={styles.imageStyle}
//           />
//         ) : null}
//         <Text style={styles.text}> {this.state.user_name} </Text>
//         <Text> {this.state.token} </Text>
//      {/* <Button title='logout' onPress={this.onLogout11()}/>  */}
     
//         <LoginButton
//           readPermissions={['public_profile']}
//           onLoginFinished={(error, result) => {
//             if (error) {
//               alert(error);
//               alert('login has error: ' + result.error);
//             } else if (result.isCancelled) {
//               alert('login is cancelled.');
//             } else {
//               AccessToken.getCurrentAccessToken().then(data => {
//               //  alert(data.accessToken.toString());
 
//                 const processRequest = new GraphRequest(
//                   '/me?fields=name,picture.type(large)',
//                   null,
//                   this.get_Response_Info
//                 );
//                 // Start the graph request.
//                 new GraphRequestManager().addRequest(processRequest).start();
//               });
//             }
//           }}
//           onLogoutFinished={this.onLogout}
//         />
     
//       </View>
//     );
//   }
// }
 
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//   },
//   text: {
//     fontSize: 20,
//     color: '#000',
//     textAlign: 'center',
//     padding: 20,
//   },
//   imageStyle: {
//     width: 200,
//     height: 300,
//     resizeMode: 'contain',
//   },
// });



import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Image, Button,
  ActivityIndicator,
  TouchableOpacity,
} from 'react-native';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
 import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
 import AsyncStorage from '@react-native-community/async-storage';
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      gettingLoginStatus: true,
      user_name: '',
      token: '',
      profile_pic: '',
    };
  }
 
  componentDidMount() {
    //initial configuration
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId: '323867318004-vmgm4t4crc2i1sn7cknh80tkj2rsnc56.apps.googleusercontent.com',
    });
    //Check if user is already signed in
    this._isSignedIn();
  }
 
  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      alert('User is already signed in');
      //Get the User details as user is already signed in
      this._getCurrentUserInfo();
    } else {
      //alert("Please Login");
      console.log('Please Login');
    }
    this.setState({ gettingLoginStatus: false });
  };
 
  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        alert('User has not signed in yet');
        console.log('User has not signed in yet');
      } else {
        alert("Something went wrong. Unable to get user's info");
        console.log("Something went wrong. Unable to get user's info");
      }
    }
  };
 
  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };
 
  _signOut = async () => {
    //Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null }); // Remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

    get_Response_Info = async(error, result) => {
    if (error) {
      //Alert for the Error
      Alert.alert('Error fetching data: ' + error.toString());
    } else {
      //response alert
    //  alert(JSON.stringify(result));
      this.setState({ user_name: 'Welcome' + ' ' + result.name });
      this.setState({ token: 'User Token: ' + ' ' + result.id });
      this.setState({ profile_pic: result.picture.data.url });
       await AsyncStorage.setItem('userName', result.name);
        const value = await AsyncStorage.getItem('userName');
    //    console.log('value');
    //    console.log(value);
    //     console.log('value');
    }
  };
 
 
  onLogout = () => {
    //Clear the state after logout
    this.setState({ user_name: null, token: null, profile_pic: null });
  };
  
 
  render() {
    //returning Loader untill we check for the already signed in user
    if (this.state.gettingLoginStatus) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    } else {
      if (this.state.userInfo != null) {
        //Showing the User detail
        return (
          <View style={styles.container}>
            <Image
              source={{ uri: this.state.userInfo.user.photo }}
              style={styles.imageStyle}
            />
            <Text style={styles.text}>
              Name: {this.state.userInfo.user.name}{' '}
            </Text>
            <Text style={styles.text}>
              Email: {this.state.userInfo.user.email}
            </Text>
            <TouchableOpacity style={styles.button} onPress={this._signOut}>
              <Text>Logout</Text>
            </TouchableOpacity>
          </View>
        );
      } else {
        //For login showing the Signin button
        return (
          <View style={styles.container}>
            <GoogleSigninButton
              style={{ width: 240, height: 40, marginBottom:10 }}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
              onPress={this._signIn}
            />
            <LoginButton
          readPermissions={['public_profile']}
          onLoginFinished={(error, result) => {
            if (error) {
              alert(error);
              alert('login has error: ' + result.error);
            } else if (result.isCancelled) {
              alert('login is cancelled.');
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
              //  alert(data.accessToken.toString());
 
                const processRequest = new GraphRequest(
                  '/me?fields=name,picture.type(large)',
                  null,
                  this.get_Response_Info
                );
                // Start the graph request.
                new GraphRequestManager().addRequest(processRequest).start();
              });
            }
          }}
          onLogoutFinished={this.onLogout}
        />
          </View>
        );
      }
    }
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 30,
  },
});