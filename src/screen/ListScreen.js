import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ListScreen from './ListScreen';
export default class List extends Component {
  render() {
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
          
          <View style={styles.tabView}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('MapScreen')}>
          <View style={[styles.subTab,{backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={[styles.fontStyle,{color:'white'}]}>MAP</Text>
          </View>
          </TouchableOpacity>
          <View style={styles.subTab}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={styles.fontStyle}>LIST</Text>
          </View>
          
          <TouchableOpacity onPress={() => this.props.navigation.navigate('FavoriteScreen')}>
          <View style={[styles.subTab,{width:wp('25.3%'),backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={[styles.fontStyle,{color:'white'}]}>FAVORITE</Text>
          </View>
          </TouchableOpacity>
          </View >
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  headerView: {
      position: 'absolute',
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "space-between",
    paddingHorizontal: 10,
    alignItems: "center",
    height:  hp('9%'),
    width:'100%'
  },
  tabView : {
    position: 'absolute',
    height: hp('9%'),
   margin: wp('10%'),
   marginBottom:hp('5%'),
   marginTop:hp('85%'),
    //justifyContent: "flex-end",
    backgroundColor: '#034d94',
    borderRadius:35,
    flexDirection:'row'
  },
   menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  searchBox : {
    position: 'absolute',
    height: hp('7%'),
   margin: wp('10%'),
   marginBottom:hp('5%'),
   marginTop:hp('11.5%'),
    //justifyContent: "flex-end",
    backgroundColor: 'white',
    borderRadius:35,
    flexDirection:'row',
    width:'80%'
  },
  subTab: {
  width:wp('23.1%'),
  margin:wp('1.5%'),
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'white',
  borderRadius:33
  },
  tapImage:{
    height:'20%',
  },
  fontStyle:{
    fontSize:12
  },
  map: {
    marginTop:hp('9%'),
   position: 'absolute',
   left: 0,
   right: 0,
  height:hp('72%')
 },
});