import React from 'react';
 import { View, StyleSheet, Text, Alert, Image, Button, ScrollView, TextInput } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import Toolbar from '../component/Toolbar'
 import Slideshow from 'react-native-slideshow';
export default class PropertyInfo extends React.Component {
  constructor() {
 
    super();
 
    this.state = {
      position: 1,
      interval: null,
      dataSource: [
        {
          url: 'https://reactnativecode.000webhostapp.com/images/dahlia-red-blossom-bloom-60597.jpeg',
        },
         {
           url: 'https://reactnativecode.000webhostapp.com/images/flower-impala-lily-floral-plant-65653.jpeg',
        },
         {
          url: 'https://reactnativecode.000webhostapp.com/images/flowers-background-butterflies-beautiful-87452.jpeg',
        },
      ],
    };
 
  }
 
  componentWillMount() {
    this.setState({
      interval: setInterval(() => {
        this.setState({
          position: this.state.position === this.state.dataSource.length ? 0 : this.state.position + 1
        });
      }, 5000)
    });
  }
 
  componentWillUnmount() {
    clearInterval(this.state.interval);
  }
  render() {
    //Image Slider
    return (
        <View style={styles.container}> 
         <Toolbar title={"Property Information"} />
        
           <View style={styles.mainView}>
        
       <Slideshow
          dataSource={this.state.dataSource}
          position={this.state.position}
          onPositionChanged={position => this.setState({ position })}
          indicatorColor={'transparent'}
          indicatorSelectedColor={'transparent'}
          height={hp('25%')}
           />
           <View style={{ flexDirection:'row', alignItems:'center',}}>
              <View style={styles.leftSideText}>
           
            <Text style={styles.availabilityText}>2 Beds | 1 Bath | 900 sq ft</Text>
            <Text style={styles.addresstext}>Chicago, IL, USA</Text>
              <View style={{flexDirection:'row', marginTop:hp('1%')}}>
               <Image style={styles.clockImg} source={require('../image/clock.png')}/>
               <Text >Posted 7 days ago</Text>
              </View>
             </View>
               <View style={styles.imageView}>
                <Image style={styles.flotingImage} source={require('../image/mapImg.png')}/>
                 </View>
                   </View>
            
            
           <View style={styles.priceOuterView}>
           <Text style={styles.priceText}>$2000</Text>
           <View style={styles.viewButton}> 
      <Text style={{fontSize:11}}>AVAILABLE  NOW</Text>
     </View>
           </View>

        <View style={styles.desciptionView}>
        <Text style={{fontSize:13}}>Proprty Description appear here  Proprty Description appear here  Proprty Description appear here  Proprty Description appear here  Proprty Description appear here  </Text>
        </View>

        <View style={styles.floatingView}>
           <View style={styles.imageView}>
                <Image style={styles.flotingImage} source={require('../image/mapImg.png')}/>
                 </View>
                 <View style={styles.imageView}>
                <Image style={styles.flotingImage} source={require('../image/call.png')}/>
                 </View>
                 </View>
               <View style={styles.offerOuterView}>
                  <View style={styles.rentApplyView}>
                <Text style={styles.whiteText}>Apply for Rent</Text>
               </View> 
               <View style={styles.makeOfferView}>
              <Text style={styles.whiteText}>Make an Offer</Text>
               </View> 
               </View> 
         </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  ScrollViewStyle:{
    width:wp('92%'), 
    height:hp('87%'),
    backgroundColor: "#ededed", 
    margin:wp('4%')
    },
     mainView:{
    height:hp('83%'),
     width:'90%',
     margin:wp('5%'), 
     backgroundColor:'white',
   // backgroundColor:'#f1f1f1',
      borderRadius:10, 
     // flex:1
},
    detailTextStyle:{
     fontSize:12.8,
      marginStart:wp('4%')
    },
      applyRent:{ 
        width:wp('40%'),
         alignItems:'center', 
         marginStart:wp('2%'), 
          justifyContent:'center'
          },
     lastButton:{
            height:hp('8%'),
             width:'90%',
              flexDirection:'row',
              backgroundColor: '#034d94',
              borderRadius:50,
         alignItems:'center',
          borderColor:'black',
           borderWidth:0.5, margin:wp('5%')
      },
      flotingImage:{
        height:hp('4%'),
         width:hp('4%'),
        },
         imageView:{
 backgroundColor:'#034d94', 
 //marginEnd:hp('2%'),
 alignItems:'center',
 justifyContent:'center',
 borderRadius:hp('3.5%'), 
 height:hp('7%'),
 width:hp('7%')
 },
  viewButton: {
        justifyContent:'center', 
        alignItems:'center',
         backgroundColor:'#cbe8d8',
          borderRadius:15,
           height:hp('3.8%'),
      borderColor:'green',
       borderWidth:2,
        marginStart:wp('12%'),
         width:wp('30%')
         },
   whiteText:{
      color:'white'
    },
rentApplyView:{
  width:'50%',
   backgroundColor:'#034d94', 
  borderBottomLeftRadius: 11,
   borderEndWidth:1,
   justifyContent:'center', 
  alignItems:'center',
   borderColor:'white'
},
makeOfferView:{
  justifyContent:'center',
   alignItems:'center',
  borderBottomRightRadius: 11,
   width:'50%',
    backgroundColor:'#008f2c',
     borderColor:'white'
},
offerOuterView:{
  height:hp('7%'), 
  marginTop:hp('2.3%'),
   flexDirection:'row'
},
floatingView:{
  height:hp('7%'),
   width:'100%', 
  flexDirection:'row',
  justifyContent: "space-between",
    paddingStart:wp('20%'),
     paddingEnd:wp('20%') 
},
desciptionView:{
  padding:wp('5%'), 
},
availabilityText:{
   marginTop:hp('4%'), 
   marginBottom:hp('1%'),
    fontSize:13
},
addresstext:{
  fontWeight:"bold", fontSize:15
},
leftSideText:{
  height:hp('16.8'),
   width:'65%',
    marginStart:wp('6%'), 
},
clockImg:{
  height:hp('3%'),
   width:hp('3%'),
    marginEnd:wp('2%') 
},
priceOuterView:{
  height:hp('10%'), 
   width:'88%',
    marginStart:wp('6%'),
     marginEnd:wp('6%'),
      borderTopWidth:0.5, 
      borderBottomWidth:0.5,
        justifyContent: "space-between",
         flexDirection:'row',
          alignItems:'center'
},
priceText:{
  fontWeight:'bold',
   fontSize:20,
    color:'#034d94', 
}

});

