import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
// import {
//   LoginButton,
//   AccessToken,
//   GraphRequest,
//   GraphRequestManager,
// } from 'react-native-fbsdk';
import { LoginManager } from 'react-native-fbsdk';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
export default class Login extends Component {
    constructor(props) {
    super(props);

    this.state = {
      password:'',
     username:'',
      userInfo: null,
      gettingLoginStatus: true,
      user_name: '',
      token: '',
      profile_pic: '',
    };   
  }
 
  componentDidMount() {
    //initial configuration
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId: '323867318004-vmgm4t4crc2i1sn7cknh80tkj2rsnc56.apps.googleusercontent.com',
    });
    //Check if user is already signed in
    this._isSignedIn();
  }
 
  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      alert('User is already signed in');
      //Get the User details as user is already signed in
      this._getCurrentUserInfo();
    } else {
      //alert("Please Login");
      console.log('Please Login');
    }
    this.setState({ gettingLoginStatus: false });
  };
 
  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        alert('User has not signed in yet');
        console.log('User has not signed in yet');
      } else {
        alert("Something went wrong. Unable to get user's info");
        console.log("Something went wrong. Unable to get user's info");
      }
    }
  };
 
  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };
 
  _signOut = async () => {
    //Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null },function(){
        console.log('successfully logout');
      }); // Remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

 _fbAuth() {
  LoginManager.logInWithPermissions(["public_profile"]).then(
  function(result) {
    if (result.isCancelled) {
      console.log("Login cancelled");
    } else {
      console.log(
        "Login success with permissions: " +
          result.grantedPermissions.toString()
      );
    }
  },
  function(error) {
    console.log("Login fail with error: " + error);
  }
);
}


  onLogout = () => {
    //Clear the state after logout
    this.setState({ user_name: null, token: null, profile_pic: null });
  };
  
 
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <View style={styles.mainView}>
           <View style={{ height:hp('72%')}}>

           <View style={styles.logoStyle}>
            <Image
          style={styles.logo}
          source={require('../image/logo.png')}
          />
          </View>

          <View style={styles.titleView}>
          <Text style={styles.titleText}>Sign In</Text>
          </View>

              <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Username</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          //placeholder="Enter Email"
          onChangeText={(username) => this.setState({username})}
          value={this.state.username}
        />
        <View style={styles.textinputBorder}></View>
      
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Password</Text>
             </View>
              <View style={[styles.requiredField,{justifyContent:'space-between', height:50, width:'100%', }]}>
              <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
         // placeholder="Enter Email"
          onChangeText={(email) => this.setState({email})}
          value={this.state.password}
        />
                 <Image style={styles.hideShow} source={require('../image/eye.png')}/>
              </View>
            <View style={styles.textinputBorder}></View>

        <TouchableOpacity style={styles.forgotPass}>
        <Text style={{color:'#034d94'}}> Forgot password?</Text>
        </TouchableOpacity>

        <View style={styles.signUpBtn}>
        <Text style={styles.signInText}> SIGN IN</Text>
        </View>

       <View style={styles.socialView}>
       <TouchableOpacity style={styles.facebookView}
           onPress={this._fbAuth}
       >
        <Image
          style={styles.fbLogo}
          source={require('../image/facebook.png')}
          />
       <Text style={{fontSize:13, fontWeight:'bold', color:'white'}}>Facebook</Text>
       </TouchableOpacity>

     <TouchableOpacity style={[styles.facebookView,{backgroundColor:'#f43c47'}]}
      onPress={this._signIn}>
        <Image
          style={styles.fbLogo}
          source={require('../image/google.png')}
          />
       <Text style={{fontSize:13, fontWeight:'bold', color:'white'}}>Google</Text>
       </TouchableOpacity>
        </View>

 <TouchableOpacity style={styles.button} onPress={this._signOut}>
              <Text>Logout</Text>
            </TouchableOpacity>

     <View style={styles.lastRow}>
     <Text > Not a member? </Text>
        <TouchableOpacity  >
              <Text style={{color:'green'}}>Sign Up</Text>
                </TouchableOpacity>
               
          
        </View>

        </View>
       </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:hp('84%'),
     width:'86%',
     margin:'7%',
     marginTop:hp('6%'),
     marginBottom:hp('6%'), 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1},
      logoStyle:{ 
           width:'100%', 
           height: hp('12%'),
            justifyContent:'center',
             alignItems:'center'
             },
 nameStyle:{
    marginTop:hp('5%'), 
     marginStart:wp('10%')
 },
 textInput:{
     marginTop:-hp('1.3%'),
      marginStart:wp('10%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%'),
      marginBottom:hp('2%')
      },
  buttonStyle:{height:hp('8%'),
       width:'100%',
       borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
       backgroundColor:'#034d94',
        // marginTop:hp('24%'),
        //justifyContent:'flex-end',
     alignItems:'center'
  },
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
   },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
     nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#008f2c',
     width:'84%',
      marginStart:'8%', 
     marginEnd:'8%',
  marginTop:hp('5%'),
   marginBottom:hp('4%'), 
   borderRadius:10,
    justifyContent:'center',
     alignItems:'center'
     },
     lastRow:{
           flexDirection:'row',
            marginTop:hp('1%'), 
            width:'100%', 
            height:hp('6%'),
         justifyContent:'center', 
         alignItems:'center',
         },
    logo:{
      width: '45%',
       height: hp('8%'), 
    },
    titleView:{
      justifyContent:'center',
       alignItems:'center',
        height:hp('11%')
    },
   titleText:{
     justifyContent:'center',
      alignItems:'center',
       height:hp('11%')
   },
   hideShow:{
     height:hp('3%'),
      width:hp('3%'),
       marginEnd:wp('10%'),
   },
   forgotPass:{
     height:hp('3%'),
      marginStart:wp('44%'),
       marginTop:-hp('0.5%'), 
   },
   signInText:{
     fontSize:18,
      fontWeight:"bold",
       color:'white'
   },
   socialView:{
     flexDirection:'row',
      height:hp('7%'), 
      marginBottom:hp('2%'),
       justifyContent:'space-between',
       marginStart:wp('8%'),
        marginEnd:wp('8%')
   },
  facebookView:{
    height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('32%'),
      borderRadius:10,
     justifyContent:'space-between',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'), paddingEnd:wp('2%')
  },
 fbLogo:{
   width: hp('4%'), height: hp('4%'), 
 },
button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 30,
  },

});


