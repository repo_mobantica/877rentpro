import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import ListScreen from './ListScreen';
import FavoriteScreen from './FavoriteScreen';
import MapView, { Marker } from 'react-native-maps';
import IconBadge from "react-native-icon-badge";
import Geocoder from 'react-native-geocoder';
import { Searchbar } from 'react-native-paper';
 import Toolbar from '../component/Toolbar'
const coordsArray =[
  {
              latitude: 18.5635,
              longitude:73.7817,
            },
            {
              latitude: 18.5698,
              longitude:73.7739,
            },
            {
              latitude: 18.5730,
              longitude:73.7776,
            },
            {
              latitude: 18.5672,
              longitude:73.7863,
            }
]
export default class Map extends Component {
   state = {
   focusedLocation: {
     latitude: 34.052235,
     longitude: -118.243683,
     latitudeDelta: 0.0122,
     longitudeDelta:Dimensions.get("window").width / Dimensions.get("window").height*0.0122
   },
   locationChoosen:false,
   search: '',  
   selectedMarkerIndex:'',
   showDialog:false
 }

 picKLocationHandler = event =>{
   const coords = event.nativeEvent.coordinate;
   this.map.animateToRegion({
     ...this.state.focusedLocation,
     latitude: coords.latitude,
     longitude: coords.longitude
   });
   this.setState(prevState => {
      return{
        focusedLocation: {
          ...prevState.focusedLocation,
          latitude: coords.latitude,
          longitude:coords.longitude
        },
        locationChoosen:true
      };
   });
 }
 //'lat',res.position[lat)
 updateSearch = search => {
  this.setState({ search });
};
 searchLocation(){
      Geocoder.geocodeAddress(this.state.search).then(res => {
        // console.log('aaaaaaaaa');
        // console.log(res );
        // console.log('aaaaaaaaa');
        // console.log('lng',res[0].position.lng,'lat',res[0].position.lat );
        // console.log('aaaaaaaaa');
       
        this.map.animateToRegion({
          ...this.state.focusedLocation,
          latitude: res[0].position.lat,
          longitude:res[0].position.lng
        });
        this.setState(prevState => {
           return{
             focusedLocation: {
               ...prevState.focusedLocation,
               latitude: res[0].position.lat,
               longitude:res[0].position.lng
             },
             locationChoosen:false
           };
        });
      })
      .catch(err => console.log(err))
    }
    onPressMarker(e, index) {
    this.setState({selectedMarkerIndex: index,
    showDialog:true});
}
onCancelPress(){
  this.setState({
    showDialog:false});
}
  render() {
     const { search } = this.state;
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
    <Toolbar image title={"Dashboard"} />
<MapView
         style={styles.map}
         initialRegion={this.state.focusedLocation}
           onPress ={this.picKLocationHandler}

          ref = {ref => this.map =ref  }
          >
       {coordsArray.map((item, index) => (
         //key ={index}
           <MapView.Marker
            draggable
            key ={index}
            coordinate={item}
            onPress={(e) => {e.stopPropagation(); this.onPressMarker(e, index)
            // this.onMarkerPress(index)
             }}
               >
           {this.state.selectedMarkerIndex === index ?
           <Image
                style={{height:30, width:30}}
                source={require("../image/selectedLocation.png")}
              />
              :
               <Image
                style={{height:30, width:30}}
                source={require("../image/location.png")}
              />
           }
                  
         </MapView.Marker>
       ))}          
       </MapView>
         <View style={[styles.searchBox,{justifyContent:'center', alignItems:'center'}]}>
       <Searchbar
        placeholder="Select Location"
      //  onChangeText={this.searchLocation()}
        onChangeText={query => { this.setState({ search: query }, function(){
          this.searchLocation();
        }); }}
        value={this.state.search}
        placeholderTextColor= '#034d94' 
         searchIcon={<View><Image source={require("../image/location.png")} style={{ width: 9, height: 9}} /></View>}
        style={{backgroundColor:'transparent', height:hp('6.8%'), borderRadius:35, width:'100%'}}
      />
         </View>
         
       {
         this.state.showDialog ?
         <View style={{height:hp('25.9%'), backgroundColor:'white',marginTop:hp('70%'),
          marginBottom:0, marginStart:wp('8%'), marginEnd:wp('8%')}}>

           <Image
          style={{ borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,width: '100%', height: hp('11.8%')}}
          source={require('../image/room.png')}
       />
      <TouchableOpacity
      style={{height:36, width:36, borderRadius:18, backgroundColor:'white', marginTop:-hp('13.5%'),
         marginStart:wp('77%'), justifyContent:'center', alignItems:'center'}}
              onPress={() => this.onCancelPress()}>
      
         <Image
          style={{width: 10, height:10}}
          source={require('../image/cancel.png')}
       />
       
         </TouchableOpacity>

        <View style={styles.firstRow}>
    <Text style={{color:'#034d94', fontWeight:'bold'}}>$210</Text>
    <Image
          style={{width: 15, height:15}}
          source={require('../image/heart.png')}
       />
       
    </View>
    <Text style={{color:'#939393', marginStart:wp('3.7%'), fontSize:10}}>4 Bed | 3 Balcony | 3 Bathrooms</Text>
     <Text style={{color:'black', marginStart:wp('3.7%'), fontWeight:'bold',}}>chicago, IL, USA</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 18, height:18}}
          source={require('../image/clock.png')}
       />
     <Text style={{color:'#939393', marginStart:wp('1.6%'), fontSize:10 }}>Applied, Yesterday at 12:09 PM</Text>
      </View>
       
     </View>
     :
   <View style={styles.tabView}>
          <View style={styles.subTab}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={styles.fontStyle}>MAP</Text>
          </View>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('ListScreen')}>
          <View style={[styles.subTab,{backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={[styles.fontStyle,{color:'white'}]}>LIST</Text>
          </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('FavoriteScreen')}>
          <View style={[styles.subTab,{width:wp('25.3%'),backgroundColor:'#034d94'}]}>
          <Image  style={styles.menuIcon}
           source={require("../image/map.png")}/>
          <Text style={[styles.fontStyle,{color:'white'}]}>FAVORITE</Text>
          </View>
            </TouchableOpacity>
          </View > 
       } 

         
    {/* <View style={{height:hp('25.9%'), backgroundColor:'white',marginTop:hp('70%'),
          marginBottom:0, marginStart:wp('8%'), marginEnd:wp('8%')}}>

           <Image
          style={{ borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,width: '100%', height: hp('11.8%')}}
          source={require('../image/room.png')}
       />
      <TouchableOpacity 
      style={{height:34, width:34, borderRadius:17, backgroundColor:'white', marginTop:-100,
         marginStart:wp('72%'), justifyContent:'center', alignItems:'center'}}
              onPress={() => this.onCancelPress()}>
      {/* <View style={{height:32, width:32, borderRadius:16, backgroundColor:'red', marginTop:-100,
         marginStart:wp('72%'), justifyContent:'center', alignItems:'center'}}> */}
         {/* <Image
          style={{width: 10, height:10}}
          source={require('../image/cancel.png')}
       />
        
         </TouchableOpacity>

        <View style={styles.firstRow}>
    <Text style={{color:'#034d94', fontWeight:'bold'}}>$210</Text>
    <Image
          style={{width: 15, height:15}}
          source={require('../image/heart.png')}
       />
       
    </View>
    <Text style={{color:'#939393', marginStart:wp('3.7%'), fontSize:10}}>4 Bed | 3 Balcony | 3 Bathrooms</Text>
     <Text style={{color:'black', marginStart:wp('3.7%'), fontWeight:'bold',}}>chicago, IL, USA</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 18, height:18}}
          source={require('../image/clock.png')}
       />
     <Text style={{color:'#939393', marginStart:wp('1.6%'), fontSize:10 }}>Applied, Yesterday at 12:09 PM</Text>
      </View>
       
     </View>
      */} 

         
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  headerView: {
      position: 'absolute',
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "space-between",
    //paddingHorizontal: 10,
    alignItems: "center",
    height:  hp('9%'),
    width:'100%'
  },
   headerIcon: {
    height: "30%",
    resizeMode: "center",
    width: 35,
   // marginEnd: 5
  },
  tabView : {
    position: 'absolute',
    height: hp('9%'),
   margin: wp('10%'),
   marginBottom:hp('5%'),
   marginTop:hp('85%'),
    //justifyContent: "flex-end",
    backgroundColor: '#034d94',
    borderRadius:35,
    flexDirection:'row'
  },
  searchBox : {
    position: 'absolute',
    height: hp('7%'),
   margin: wp('10%'),
   marginBottom:hp('5%'),
   marginTop:hp('11.5%'),
    //justifyContent: "flex-end",
    backgroundColor: 'white',
    borderRadius:35,
    flexDirection:'row',
    width:'80%'
  },
  subTab: {
  width:wp('23.1%'),
  margin:wp('1.5%'),
  justifyContent:'center',
  alignItems:'center',
  backgroundColor:'white',
  borderRadius:33
  },
  tapImage:{
    height:'20%',
  },
  fontStyle:{
    fontSize:12
  },
  map: {
    marginTop:hp('9%'),
   position: 'absolute',
   left: 0,
   right: 0,
  height:hp('72%')

 },
 firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:hp('10%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    alignItems:'center'
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
});