import React, { Component } from 'react';
import { Text, View } from 'react-native';

export default class Two extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Text>Hello, world! two</Text>
      </View>
    );
  }
}
