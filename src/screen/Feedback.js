import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import CheckBox from 'react-native-check-box'
import ToolbarWithNavAndHome from '../component/ToolbarWithNavAndHome';
export default class Feedback extends Component {
    constructor(props) {
    super(props);

    this.state = {
      tenant:false,
      manager:false,
      landlord:false,
      broker:false,
      firstName:'',
      lastName:'',
      address:'',
      email:'',
      phone:'',
      message:'',
    };   
  }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <ToolbarWithNavAndHome title="Feedback " />    
           <View style={styles.mainView}>
           <ScrollView >
         
          <View style={[styles.iconRow,{marginTop:hp('3%')}]}>
           <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              tenant:!this.state.tenant
           })
               }}
           isChecked={this.state.tenant}
          />
             <Text style={styles.checkboxText}>Prospective Tenant</Text>
            <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              manager:!this.state.manager
           })
               }}
          isChecked={this.state.manager}
          />
               <Text style={styles.checkboxText}>Property Manager</Text>
          </View>

           <View style={[styles.iconRow,{marginBottom:hp('3%')}]}>
           <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              landlord:!this.state.landlord
           })
               }}
          isChecked={this.state.landlord}
          />
            <Text style={styles.checkboxText}>Landlord</Text>
            <CheckBox
            style={{ paddingRight: 5}}
             checkBoxColor={'#034d94'}
                    checkedCheckBoxColor={'#034d94'}
            onClick={()=>{
           this.setState({
              broker:!this.state.broker
           })
               }}
          isChecked={this.state.broker}
          />
               <Text style={styles.checkboxText}>Broker Agent</Text>
          </View>

            <Text style={styles.nameStyle}>Address</Text>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="Address here"
          onChangeText={(address) => this.setState({address})}
          value={this.state.address}
          multiline={true}
        />
        </View>
        <View style={styles.textinputBorder}></View>

           <Text style={styles.nameStyle}>First Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter First Name"
          onChangeText={(firstName) => this.setState({firstName})}
          value={this.state.firstName}
        />
        <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>Last Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Last Name"
          onChangeText={(lastName) => this.setState({lastName})}
          value={this.state.lastName}
        />
        <View style={styles.textinputBorder}></View>
       
        <Text style={styles.nameStyle}>Email</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Email here"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />
        <View style={styles.textinputBorder}></View>
       
        <Text style={styles.nameStyle}>Phone</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter your Phone number here"
          onChangeText={(phone) => this.setState({phone})}
          value={this.state.phone}
        />
        <View style={styles.textinputBorder}></View>

         <Text style={styles.nameStyle}>Message</Text>
             <View style={styles.textInputView}>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
             style = {{ flex: 1, textAlignVertical:'top' }}
            numberOfLines={3}
          placeholder="write here message"
          onChangeText={(message) => this.setState({message})}
          value={this.state.message}
          multiline={true}
        />
        </View>
        <View style={styles.textinputBorder}></View>
         
       </ScrollView>
      <TouchableOpacity style={styles.lastButton}>
      <Text style={styles.sendText}> Send</Text>
     </TouchableOpacity>
     
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:'100%',
     width:'90%',
     margin:wp('5%'), 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1
},

iconRow:{
  height:hp('5%'),
   marginTop:hp('1%'), 
    width:'100%',
     flexDirection:'row', 
     alignItems:'center',
      //backgroundColor:'red',
     paddingStart:'5%', paddingEnd:'5%'
     },
    checkboxText:{
      width:'42%',
      fontSize:13,
       color:'gray'
    },
    nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
   textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
   },
   lastButton:{
     width:'100%',
       marginTop:hp('2%'),
        height:'8%', 
        borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
        backgroundColor:'#034d94', 
        justifyContent:'center', 
        alignItems:'center'
   },
   sendText:{
     color:'white',
      fontWeight:'bold'
   }
});

