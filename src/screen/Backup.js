import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Image, Button,
  ActivityIndicator,
  TouchableOpacity, ScrollView, TextInput
} from 'react-native';
import {Form,Item,Input,Label,Content,Container,Icon} from "native-base"
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
 import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
 import AsyncStorage from '@react-native-community/async-storage';
 import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';
export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName:'',
      lastName:'',
      middleName:'',
      email:'',
      password:'',
      confirmPass:'',
      income:'',
      DOB:'',
      mobileNo:'',
      country:'',
      state:'',
      city:'',
    };
  }
 
  render() {
    
        return (
            <View style={styles.container}>
            <ToolbarWithLeftIcon title="Sign In" />

            <ScrollView 
            style={{ width:'100%',}}
            >
           <View style={styles.logoStyle}>
            <Image
          style={{width: '45%', height: hp('8%'), }}
          source={require('../image/logo.png')}
          />
          </View>
          <View style={[styles.logoStyle,{height:hp('13%')}]}>
          <Text style={{marginStart:wp('10%'), marginEnd:wp('10%'), fontSize:14}}>
          Sign up or Sign In to access your saved searches and faviourite or to add a rentel listing on 877rentpro.com</Text>
          </View>
          <Text style={styles.inputHeader}>General account Information</Text>
          <Form style={styles.formStyle}>
          <Item floatingLabel>
      <Label style={styles.lable}>Email *</Label>
        <Input style={styles.input} type="text"
                        name='Email'
                       value={this.state.email}
                        onChangeText={text => this.setState({ email: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>First Name</Label>
       <Input style={styles.input} type="text"
                        name='First Name'
                       value={this.state.firstName}
                    //    onChangeText={text => this.namevalidate(text)}  />
                        onChangeText={text => this.setState({ firstName: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>Middle Name</Label>
       <Input style={styles.input} type="text"
                        name='Middle Name'
                       value={this.state.middleName}
                        onChangeText={text => this.setState({ middleName: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>Last Name</Label>
        <Input style={styles.input} type="text"
                        name='Last Name'
                       value={this.state.lastName}
                        onChangeText={text => this.setState({ lastName: text })}  /> 
      </Item>
      
      <Item floatingLabel>
      <Label style={styles.lable}>Country</Label>
       <Input style={styles.input} type="text"
                        name='Country'
                       value={this.state.country}
                        onChangeText={text => this.setState({ country: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>State</Label>
        <Input style={styles.input} type="text"
                        name='State'
                       value={this.state.state}
                        onChangeText={text => this.setState({ state: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>City</Label>
        <Input style={styles.input} type="text"
                        name='City'
                       value={this.state.city}
                        onChangeText={text => this.setState({ city: text })}  /> 
      </Item>
      </Form>
      <Text style={styles.inputHeader}>Personal Informationn</Text>
           <Form style={styles.formStyle}>
          <Item floatingLabel>
      <Label style={styles.lable}>Date of Birth</Label>
        <Input style={styles.input} type="text"
                        name='Date of Birth'
                       value={this.state.DOB}
                        onChangeText={text => this.setState({ DOB: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>First Name</Label>
       <Input style={styles.input} type="text"
                        name='First Name'
                       value={this.state.firstName}
                    //    onChangeText={text => this.namevalidate(text)}  />
                        onChangeText={text => this.setState({ firstName: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>Annual Income</Label>
       <Input style={styles.input} type="text"
                        name='Annual Income'
                       value={this.state.income}
                        onChangeText={text => this.setState({ income: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>Mobile Number</Label>
        <Input style={styles.input} type="text"
                        name='Mobile Number'
                       value={this.state.mobileNo}
                        onChangeText={text => this.setState({ mobileNo: text })}  /> 
      </Item>
      
      <Item floatingLabel>
      <Label style={styles.lable}>Password</Label>
       <Input style={styles.input} type="text"
                        name='Password'
                       value={this.state.password}
                        onChangeText={text => this.setState({ password: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>Confirm Password</Label>
        <Input style={styles.input} type="text"
                        name='Confirm Password'
                       value={this.state.confirmPass}
                        onChangeText={text => this.setState({ confirmPass: text })}  /> 
      </Item>

      <Item floatingLabel>
      <Label style={styles.lable}>City</Label>
        <Input style={styles.input} type="text"
                        name='City'
                       value={this.state.city}
                        onChangeText={text => this.setState({ city: text })}  /> 
      </Item>
      </Form>
       
         
           <View style={{width:'80%', height: hp('5.5%'), justifyContent:'center', alignItems:'center', marginTop:hp('2%'), marginBottom:hp('0.5%'),
            backgroundColor:'green', marginStart:wp('10%'), marginEnd:wp('10%'), borderRadius:5}}>
            <Text style={{color:'white'}}>Sign In</Text>
           </View>
           
         <View style={{flexDirection:'row', width:'80%',marginStart:wp('10%'), marginEnd:wp('10%'), height: hp('5%'),alignItems:'center'}}>
         <View style={styles.orLine}></View>
         <View style={styles.orView}>
         <Text >or</Text>
         </View>
            <View style={styles.orLine}></View>
         </View>
          {this.state.userInfo != null ?
         <TouchableOpacity style={styles.button} onPress={this._signOut}>
              <Text style={{color:'white'}}>Logout</Text>
            </TouchableOpacity>:
         
            <GoogleSigninButton
              style={{marginStart:wp('8%'), marginEnd:wp('8%'), height:hp('6.5%'), width:wp('84%'),}}
              //size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
             onPress={this._signIn}
            />
       
            }
             <View style={{ marginStart:wp('10%'), marginEnd:wp('10%'), height:hp('5.5%'), marginBottom:hp('2%'), marginTop:hp('1.5%')}}>
            <LoginButton
          readPermissions={['public_profile']}
          onLoginFinished={(error, result) => {
            if (error) {
              alert(error);
              alert('login has error: ' + result.error);
            } else if (result.isCancelled) {
              alert('login is cancelled.');
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
              //  alert(data.accessToken.toString());
 
                const processRequest = new GraphRequest(
                  '/me?fields=name,picture.type(large)',
                  null,
                  this.get_Response_Info
                );
                // Start the graph request.
                new GraphRequestManager().addRequest(processRequest).start();
              });
            }
          }}
          onLogoutFinished={this.onLogout}
        />
        </View>
        </ScrollView>
          </View>
        );
     
    
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#034d94',
   marginStart:wp('10%'), marginEnd:wp('10%'),
   justifyContent:'center',
    width: '80%', height:hp('6.8%'), marginBottom:10
  },
  googleButton:{
     width: wp('80%'),
     height:hp('6.8%'),
      marginBottom:10,
      marginStart:wp('10%'), 
      marginEnd:wp('10%')
      },
  orLine:{
    backgroundColor:'black',
     height:1, width:'45%'
     },
     orView:{
       justifyContent:'center',
        alignItems:'center', 
         width:'10%'
         },
         lastRow:{
           flexDirection:'row',
            marginTop:hp('3%'), 
            width:'100%', 
            height:hp('6%'),
         justifyContent:'center', 
         alignItems:'center',
         },
         logoStyle:{ 
           width:'100%', 
           height: hp('11%'),
            justifyContent:'center',
             alignItems:'center'
             },
     input :{
       color:"black",
        fontSize:16,
     marginTop:-10, marginBottom:0 ,
        width:wp('84%')
     },
     lable:{
       marginTop:-10, marginBottom:0 
     },
     inputHeader:{ 
       fontSize:15, 
       fontWeight:'bold',
        marginStart:wp('8%'), 
       marginBottom:hp('2.5%'),
       marginTop:hp('2.5%'),
       },
       formStyle:{
         marginTop:-hp('2%'),
          marginStart:wp('8%'),
        marginEnd:wp('8%'),
        }
});

------------------------------------------login------------------------------
// import React, { Component } from 'react';
// import { View, StyleSheet, Text, Alert, Image, Button } from 'react-native';
// import {
//   LoginButton,
//   AccessToken,
//   GraphRequest,
//   GraphRequestManager,
// } from 'react-native-fbsdk';
//  import AsyncStorage from '@react-native-community/async-storage';
// export default class Login extends Component {
//   constructor() {
//     super();
//     //Setting the state for the data after login
//     this.state = {
//       user_name: '',
//       token: '',
//       profile_pic: '',
//     };
//   }
 
//   get_Response_Info = async(error, result) => {
//     if (error) {
//       //Alert for the Error
//       Alert.alert('Error fetching data: ' + error.toString());
//     } else {
//       //response alert
//     //  alert(JSON.stringify(result));
//       this.setState({ user_name: 'Welcome' + ' ' + result.name });
//       this.setState({ token: 'User Token: ' + ' ' + result.id });
//       this.setState({ profile_pic: result.picture.data.url });
//        await AsyncStorage.setItem('userName', result.name);
//         const value = await AsyncStorage.getItem('userName');
//     //    console.log('value');
//     //    console.log(value);
//     //     console.log('value');
//     }
//   };
 
 
//   onLogout = () => {
//     //Clear the state after logout
//     this.setState({ user_name: null, token: null, profile_pic: null });
//   };
//    onLogout11 = () => {
//     //Clear the state after logout
//     this.setState({ user_name: null, token: null, profile_pic: null });
//   };
 
//   render() {
//     return (
//       <View style={styles.container}>
//         {this.state.profile_pic ? (
//           <Image
//             source={{ uri: this.state.profile_pic }}
//             style={styles.imageStyle}
//           />
//         ) : null}
//         <Text style={styles.text}> {this.state.user_name} </Text>
//         <Text> {this.state.token} </Text>
//      {/* <Button title='logout' onPress={this.onLogout11()}/>  */}
     
//         <LoginButton
//           readPermissions={['public_profile']}
//           onLoginFinished={(error, result) => {
//             if (error) {
//               alert(error);
//               alert('login has error: ' + result.error);
//             } else if (result.isCancelled) {
//               alert('login is cancelled.');
//             } else {
//               AccessToken.getCurrentAccessToken().then(data => {
//               //  alert(data.accessToken.toString());
 
//                 const processRequest = new GraphRequest(
//                   '/me?fields=name,picture.type(large)',
//                   null,
//                   this.get_Response_Info
//                 );
//                 // Start the graph request.
//                 new GraphRequestManager().addRequest(processRequest).start();
//               });
//             }
//           }}
//           onLogoutFinished={this.onLogout}
//         />
     
//       </View>
//     );
//   }
// }
 
// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//   },
//   text: {
//     fontSize: 20,
//     color: '#000',
//     textAlign: 'center',
//     padding: 20,
//   },
//   imageStyle: {
//     width: 200,
//     height: 300,
//     resizeMode: 'contain',
//   },
// });



import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  Image, Button,
  ActivityIndicator,
  TouchableOpacity, TextInput
} from 'react-native';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from 'react-native-google-signin';
 import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
 import AsyncStorage from '@react-native-community/async-storage';
 import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
 import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userInfo: null,
      gettingLoginStatus: true,
      user_name: '',
      token: '',
      profile_pic: '',
    };
  }
 
  componentDidMount() {
    //initial configuration
    GoogleSignin.configure({
      //It is mandatory to call this method before attempting to call signIn()
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      // Repleace with your webClientId generated from Firebase console
      webClientId: '323867318004-vmgm4t4crc2i1sn7cknh80tkj2rsnc56.apps.googleusercontent.com',
    });
    //Check if user is already signed in
    this._isSignedIn();
  }
 
  _isSignedIn = async () => {
    const isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) {
      alert('User is already signed in');
      //Get the User details as user is already signed in
      this._getCurrentUserInfo();
    } else {
      //alert("Please Login");
      console.log('Please Login');
    }
    this.setState({ gettingLoginStatus: false });
  };
 
  _getCurrentUserInfo = async () => {
    try {
      const userInfo = await GoogleSignin.signInSilently();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        alert('User has not signed in yet');
        console.log('User has not signed in yet');
      } else {
        alert("Something went wrong. Unable to get user's info");
        console.log("Something went wrong. Unable to get user's info");
      }
    }
  };
 
  _signIn = async () => {
    //Prompts a modal to let the user sign in into your application.
    try {
      await GoogleSignin.hasPlayServices({
        //Check if device has Google Play Services installed.
        //Always resolves to true on iOS.
        showPlayServicesUpdateDialog: true,
      });
      const userInfo = await GoogleSignin.signIn();
      console.log('User Info --> ', userInfo);
      this.setState({ userInfo: userInfo });
    } catch (error) {
      console.log('Message', error.message);
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User Cancelled the Login Flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Signing In');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('Play Services Not Available or Outdated');
      } else {
        console.log('Some Other Error Happened');
      }
    }
  };
 
  _signOut = async () => {
    //Remove user session from the device.
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ userInfo: null }); // Remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };

    get_Response_Info = async(error, result) => {
    if (error) {
      //Alert for the Error
      Alert.alert('Error fetching data: ' + error.toString());
    } else {
      //response alert
    //  alert(JSON.stringify(result));
      this.setState({ user_name: 'Welcome' + ' ' + result.name });
      this.setState({ token: 'User Token: ' + ' ' + result.id });
      this.setState({ profile_pic: result.picture.data.url });
       await AsyncStorage.setItem('userName', result.name);
        const value = await AsyncStorage.getItem('userName');
    //    console.log('value');
    //    console.log(value);
    //     console.log('value');
    }
  };
 
 
  onLogout = () => {
    //Clear the state after logout
    this.setState({ user_name: null, token: null, profile_pic: null });
  };
  
 
  render() {
    //returning Loader untill we check for the already signed in user
    if (this.state.gettingLoginStatus) {
      return (
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      );
    } else {
      
//Showing the User detail
        return (
            <View style={styles.container}>
            <ToolbarWithLeftIcon title="Sign In" />
           <View style={styles.logoStyle}>
            <Image
          style={{width: '45%', height: hp('8%'), }}
          source={require('../image/logo.png')}
          />
          </View>
          <View style={[styles.logoStyle,{height:hp('13%')}]}>
          <Text style={{marginStart:wp('10%'), marginEnd:wp('10%'), fontSize:14}}>
          Sign up or Sign In to access your saved searches and faviourite or to add a rentel listing on 877rentpro.com</Text>
          </View>

           <TextInput
          style={{
            borderWidth: 1,  // size/width of the border
            borderColor: 'gray',  // color of the border
            width:wp('80%'),
            height:hp('6%'),
            marginStart:wp('10%'),
            marginEnd:wp('10%'),
            marginBottom:hp('2%')
          }}
          placeholder="Email"
        />
        <TextInput
          style={{
            borderWidth: 1,  // size/width of the border
            borderColor: 'gray',  // color of the border
            width:wp('80%'),
            height:hp('6%'),
            marginStart:wp('10%'),
            marginEnd:wp('10%'),
            marginBottom:hp('2%')
          }}
          placeholder="Password"
        />
        <TouchableOpacity style={{height:hp('5%'), marginStart:wp('55%'), marginTop:-hp('0.5%')}}>
        <Text style={{color:'green'}}> Forgot password?</Text>
        </TouchableOpacity>
           <View style={{width:'80%', height: hp('6%'), justifyContent:'center', alignItems:'center', marginTop:hp('2%'), 
            backgroundColor:'green', marginStart:wp('10%'), marginEnd:wp('10%'),borderRadius:3}}>
            <Text style={{color:'white'}}>Sign In</Text>
         </View>
         <View style={{flexDirection:'row', width:'80%',marginStart:wp('10%'), marginEnd:wp('10%'),
         height: hp('6%'),alignItems:'center'}}>
         <View style={styles.orLine}></View>
         <View style={styles.orView}>
         <Text >or</Text>
         </View>
            <View style={styles.orLine}></View>
         </View>
          {this.state.userInfo != null ?
         <TouchableOpacity style={styles.button} onPress={this._signOut}>
              <Text style={{color:'white'}}>Logout</Text>
            </TouchableOpacity>:
           
            <GoogleSigninButton
              style={{width:wp('82%'), height: hp('7.2%'), marginStart:wp('9%'), marginEnd:wp('9%')}}
              size={GoogleSigninButton.Size.Wide}
              color={GoogleSigninButton.Color.Dark}
              onPress={this._signIn}
            />
           
            }
             <View style={{ marginStart:wp('10%'), marginEnd:wp('10%'), height:hp('6%'), marginTop:hp('2%'),}}>
            <LoginButton
          readPermissions={['public_profile']}
          onLoginFinished={(error, result) => {
            if (error) {
              alert(error);
              alert('login has error: ' + result.error);
            } else if (result.isCancelled) {
              alert('login is cancelled.');
            } else {
              AccessToken.getCurrentAccessToken().then(data => {
              //  alert(data.accessToken.toString());
 
                const processRequest = new GraphRequest(
                  '/me?fields=name,picture.type(large)',
                  null,
                  this.get_Response_Info
                );
                // Start the graph request.
                new GraphRequestManager().addRequest(processRequest).start();
              });
            }
          }}
          onLogoutFinished={this.onLogout}
        />
        </View>
        <View style={styles.lastRow}>
        <TouchableOpacity  >
              <Text style={{color:'green'}}>Sign Up</Text>
                </TouchableOpacity>
               <Text >  if you are not a member</Text>
          
        </View>
          </View>
        );
     
    }
  }
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  imageStyle: {
    width: 200,
    height: 300,
    resizeMode: 'contain',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#034d94',
   marginStart:wp('10%'), marginEnd:wp('10%'),
   justifyContent:'center',
    width: '80%', height:hp('6.8%'), marginBottom:10
  },
  googleButton:{
     width: wp('80%'),
     height:hp('6.8%'),
      marginBottom:10,
      marginStart:wp('10%'), 
      marginEnd:wp('10%')
      },
  orLine:{
    backgroundColor:'black',
     height:1, width:'45%'
     },
     orView:{
       justifyContent:'center',
        alignItems:'center', 
         width:'10%'
         },
         lastRow:{
           flexDirection:'row',
            marginTop:hp('1.5%'), 
            width:'100%', 
            height:hp('6%'),
         justifyContent:'center', 
         alignItems:'center',
         },
         logoStyle:{ 
           width:'100%', 
           height: hp('11%'),
            justifyContent:'center',
             alignItems:'center'
             }
});