import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
export default class ChangePassword extends Component {
    constructor(props) {
    super(props);

    this.state = {
      password:'',
      newPass:'',
      confirmPass:''
    };

    
  }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <ToolbarHomeImg title="Change Password" />    
           <View style={styles.mainView}>
           <View style={{ height:hp('72%')}}>
             <Text style={styles.nameStyle}>Current Password</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Current Password"
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
        <View style={styles.textinputBorder}></View>

       <Text style={styles.nameStyle}>New Password</Text>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="New Password"
          onChangeText={(newPass) => this.setState({newPass})}
          value={this.state.newPass}
        />
        <View style={styles.textinputBorder}></View>

        <Text style={styles.nameStyle}>Confirm Password</Text>
            <TextInput
          style={{ marginStart:wp('10%'),
            marginTop:-hp('1.3%')}}
           underlineColorAndroid = "transparent"
          placeholder="Confirm Password"
          onChangeText={(confirmPass) => this.setState({confirmPass})}
          value={this.state.confirmPass}
        />
        <View style={styles.textinputBorder}></View>
        </View>
        <TouchableOpacity style={styles.buttonStyle}>
        <Text style={{fontWeight:'bold', fontSize:16, color:'white', marginTop:hp('1.3%')}}> Change Password</Text>
        </TouchableOpacity>
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:hp('80%'),
     width:'86%',
     margin:'7%', 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1},
 nameStyle:{
    marginTop:hp('5%'), 
     marginStart:wp('10%')
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')},
  buttonStyle:{height:hp('8%'),
       width:'100%',
       borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
       backgroundColor:'#034d94',
        // marginTop:hp('24%'),
        //justifyContent:'flex-end',
     alignItems:'center'
  }
});


// return (
//        <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
//       <View style={styles.container}>
//                <ToolbarHomeImg title="Change Password" />
//               <View style={{marginTop: hp('25%'), marginStart:wp('15%'), marginEnd:wp('15%'), alignItems:'center'}}>
//               <View style={styles.textInputView}> 
//                <TextInput
//               style={{height: 40}}
//               placeholder="Current Password"
//               onChangeText={(password) => this.setState({password})}
//               value={this.state.password}
//              />
//                </View>

//                 <View style={styles.textInputView}> 
//                <TextInput
//               style={{height: 40}}
//               placeholder="New Password"
//               onChangeText={(newPass) => this.setState({newPass})}
//               value={this.state.newPass}
//              />
//                </View>

//                 <View style={styles.textInputView}> 
//                <TextInput
//               style={{height: 40}}
//               underlineColorAndroid="transparent"
//               placeholder="Confirm Password"
//               onChangeText={(confirmPass) => this.setState({confirmPass})}
//               value={this.state.confirmPass}
             
//              />
//                </View>

//                 <View style={{borderRadius:30, width:wp('37%'),height:hp('5.8%'), justifyContent:'center', alignItems:'center', backgroundColor:'#034d94'}}> 
//               <Text style={{color:'white', fontWeight:'bold'}}>SUBMIT</Text>
//                </View>
        
        
//       </View>
//           </View>
//            </SafeAreaView>
//     );
//   }
// }
// const styles = StyleSheet.create({
// container: {
//     flex: 1,
//    backgroundColor: "#ededed"
//   },
//   menuIcon: {
//     height: "40%",
//     resizeMode: "contain",
//     width: 30,
//     marginStart: 5
//   },
//   textInputView:{
//      alignItems:'center',
//       justifyContent:'center',
//        width:wp('70%'), 
//      height:hp('8%'),
//       backgroundColor:'white',
//        borderColor:'black',
//         borderWidth:1,
//          borderRadius:35,
//          marginBottom:hp('5%')
//   }
    
// });