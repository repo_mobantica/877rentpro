import React, { Component } from 'react'
import { AppRegistry, StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native'
import AddCreditReportOne from '../component/AddCreditReportOne';
import AddCreditReportTwo from '../component/AddCreditReportTwo';
import Swiper from 'react-native-swiper';
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const renderPagination = (index, total, context) => {

  return (
    <View >
      <Text style={{ color: 'grey' }}>
        <Text >{index + 1}</Text>/{total}
      </Text>
    </View>
  )
}

export default class AddCreditReport extends Component {

  constructor(props){
    super(props);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressPrev = this.onPressPrev.bind(this);
    this.state = {
      idxActive: 0
    }
 }

 
 onPressPrev = () => {
  const {idxActive} = this.state;
  if (idxActive > 0) {
    this.refs.swiper.scrollBy(-1)
  }
}

onPressNext = () => {
  const {idxActive} = this.state;
  console.log('active pageeee');
  console.log(this.state.idxActive);
  // Probably best set as a constant somewhere vs a hardcoded 5
  if (idxActive < 2) {
    this.refs.swiper.scrollBy(1);
  }
}
  render() {
    return (

      <View style={{flex:1}}>
        <ToolbarHomeImg title="Add Credit Report" />

        <Swiper
          style={styles.wrapper}
          renderPagination={renderPagination}
          showsButtons={false}
           showsPagination={false}
          scrollEnabled={false}
          loop={false}
          ref={'swiper'}
          dot={true}
          onIndexChanged={idxActive => this.setState({idxActive},function(){
            console.log('nextttt', this.state.idxActive);
          }) }
        >
          {/* <View style={styles.slide}> */}
            <AddCreditReportOne/>
          {/* </View> */}
           <AddCreditReportTwo/>
           </Swiper>

             {
                 this.state.idxActive === 0 ?
                  <View style={styles.footer}>
          <View style={{flexDirection:'row'}}>
          <View style={styles.indicator}></View>
          <View style={[styles.indicator,{backgroundColor:'white', borderWidth:0.5}]}></View>
         </View>
             <TouchableOpacity style={styles.nextBtn}
       onPress={this.onPressNext}
       >
       <Text style={styles.nextText}>Next</Text>
       </TouchableOpacity> 
      </View>
      :
       <View style={styles.footer}>
          <View style={{flexDirection:'row', width:'22%'}}>
          <View style={styles.indicator}></View>
           <View style={styles.indicator}></View>
         </View>
            <TouchableOpacity style={styles.nextBtn}
    //  onPress={() => {this.facebookLogin()}}
       >
       <Text style={styles.nextText}>Reset</Text>
       </TouchableOpacity>
         <View style={styles.signUpBtn}>
        <Text style={{fontSize:13, fontWeight:"bold", color:'white'}}>Save & Make Payment</Text>
        </View>
            </View> 
             }
            

      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {},

   indicator:{ 
         height:12,
          width:12,
          borderRadius:6,
          backgroundColor:'#008f2c',
         margin:wp('1%'),
       },
       nextBtn:{
         height:hp('7%'),
       backgroundColor:'#034d94',
       width:wp('25%'),
      borderRadius:10,
     justifyContent:'center',
       alignItems:'center',
       flexDirection:'row', 
       paddingStart:wp('2%'),
        paddingEnd:wp('2%')
       },
       nextText:{
         fontSize:13,
          fontWeight:'bold',
           color:'white'
           },
          footer:{
            backgroundColor:'white',
             height:'11%',
              justifyContent:'space-between', 
              flexDirection:'row',
            alignItems:'center',
            paddingStart:'7%',
           paddingEnd:'7%'
       },
         signUpBtn:{
   height:hp('7%'),
    backgroundColor:'#008f2c',
     width:'46%',
      marginStart:'4%', 
     marginEnd:'2%',
   borderRadius:10,
    justifyContent:'center',
     alignItems:'center'
     },
})