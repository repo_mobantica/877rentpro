import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
 import AsyncStorage from '@react-native-community/async-storage';
export default class SideBarDrawer extends Component {
    constructor(props) {
    super(props);
    this.state = {
     userName:''
    };  
  }
  
componentDidMount(){
   console.log('componant Did Mounttt');
  this.retrieveData();
  this.forceUpdate();
}

 async retrieveData(){
     console.log('inside retrived');
  try {
    const value = await AsyncStorage.getItem('userName');
    console.log('inside try');
    if (value !== null) {
      // We have data!!
      console.log(value);
      this.setState({
        userName:value
      },function(){
        console.log('state');
         console.log(this.state.userName);
      })
    }
  } catch (error) {
    // Error retrieving data
     console.log('inside catch');
  }
};

  render() {
    
    return (
       <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
      <View style={styles.container}>
        <View style={{flexDirection:'row', height:hp('11%'), width:'100%', 
          alignItems: "center", marginEnd:'5%', justifyContent:'center', backgroundColor:'white' }}> 
            <Image
              style={{
                resizeMode: "contain",
                height:hp('9%'),
                width: hp('12%'),
               marginStart:'5%'
              }}
              source={require("../image/logo.png")}
            />
           </View> 
        <View style={{ height:hp('16%'), width:'100%', 
          marginEnd:'5%', }}>
             <Image
              style={{
                resizeMode: "contain",
                height:'100%',
                width: '100%',
              }}
              source={require("../image/profile.png")}
            />
            <View style={{ marginTop:-hp('5.5%'), flexDirection:'row'}}>
             <Text style={{ color: 'black', fontSize: 19, fontWeight:'bold', marginStart:wp('5%'),width:'55%',}}  numberOfLines={1} > James</Text>
              {/* <Text style={{ color: 'black', fontSize: 19, fontWeight:'bold', marginStart:wp('5%'),width:'55%',}}  numberOfLines={1} > {this.state.userName}</Text> */}
             <TouchableOpacity
             style={{ marginStart:wp('6%')}}
            onPress={() => { this.props.navigation.navigate('Login') }} >
             <Text style={{fontSize:18, color:'black', }}>Logout</Text>
          </TouchableOpacity>
          </View>
        </View> 
        <View style={{backgroundColor:'#034d94',  height:'80%', paddingTop: hp('6%')}}>
      <ScrollView>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('AddCreditReport') }} >
            <Image
              style={styles.listImg}
              source={require("../image/Payment.png")}
            />
             <Text style={styles.listText}>Add Credit Report</Text>
          </TouchableOpacity>

          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('Feedback') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>Feedback</Text>
          </TouchableOpacity>

         <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('Registration') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>Registration</Text>
          </TouchableOpacity>

          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('AddNewProperty') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>Add New Property</Text>
          </TouchableOpacity>
           
           <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('ApplyForRent') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>Apply For Rent</Text>
          </TouchableOpacity>
       
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('ContactDetails') }} >
            <Image
              style={styles.listImg}
              source={require("../image/Email.png")}
            />
             <Text style={styles.listText}>Contact Details</Text>
          </TouchableOpacity>
          
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('Login') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>Sign In</Text>
          </TouchableOpacity>
        <TouchableOpacity
            style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('PendingPayment') }} >
            <Image
              style={styles.listImg}
              source={require("../image/Payment.png")}
            />
             <Text style={styles.listText}>Pending Payment</Text>
          </TouchableOpacity>

           <TouchableOpacity
             style={styles.listStyle}
           onPress={() => { this.props.navigation.navigate('TenantApplication') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
            <Text style={styles.listText}>Tenant Application</Text>
          </TouchableOpacity>
           <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('PaymentHistory') }} >
            <Image
              style={styles.listImg}
              source={require("../image/dollar.png")}
            />
             <Text style={styles.listText}>Payment History</Text>
          </TouchableOpacity>
           <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('MyProperties') }} >
            <Image
              style={styles.listImg}
              source={require("../image/Property.png")}
            />
             <Text style={styles.listText}>My Properties</Text>
          </TouchableOpacity>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('MyCreditReport') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>My Credit Report</Text>
          </TouchableOpacity>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('MyTenants') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>My Tenants</Text>
          </TouchableOpacity>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('ChangePassword') }} >
            <Image
              style={styles.listImg}
              source={require("../image/FingerPrint.png")}
            />
             <Text style={styles.listText}>Change Password</Text>
          </TouchableOpacity>
           <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('PropertyInfo') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>Property Information</Text>
          </TouchableOpacity>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('LandlordPaymentHistory') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>Landlord payment History</Text>
          </TouchableOpacity>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('OutwardPendingPayment') }} >
            <Image
              style={styles.listImg}
              source={require("../image/dollar.png")}
            />
             <Text style={styles.listText}>Outward Pending Payment</Text>
          </TouchableOpacity>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('InwardPendingPayment') }} >
            <Image
              style={styles.listImg}
              source={require("../image/dollar.png")}
            />
             <Text style={styles.listText}>Inward Pending Payment</Text>
          </TouchableOpacity>
          <TouchableOpacity
             style={styles.listStyle}
            onPress={() => { this.props.navigation.navigate('MyProfile') }} >
            <Image
              style={styles.listImg}
              source={require("../image/filter.png")}
            />
             <Text style={styles.listText}>My Profile</Text>
          </TouchableOpacity>
               
</ScrollView>
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  listStyle:{
       flexDirection: "row",
              alignItems: "center",
              marginBottom: 15,
              paddingBottom: 10,
             // borderBottomWidth: 1,
              borderBottomColor: "#d9d9d9"
  },
  listText:{
     color: 'white', fontSize: 12 , marginStart:wp('2%')
  },
  listImg:{
      resizeMode: "contain",
                height: "100%",
                width: "10%",
                marginStart:wp('5%')
  }
    
});

//  return (
//        <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
//       <View style={styles.container}>
//         <View style={{flexDirection:'row', height:hp('20%'), width:'100%', 
//           alignItems: "center", marginEnd:'5%', }}> 
//             <Image
//               style={{
//                 resizeMode: "contain",
//                 height:hp('12%'),
//                 width: hp('12%'),
//                marginStart:'5%'
//               }}
//               source={require("../image/logo.png")}
//             />
//             <Text style={{ color: '#034d94', fontSize: 16, fontWeight:'bold', marginStart:'5%',  }}>
//              877RentPro.com</Text>
//         </View> 
//         <View style={{flexDirection:'row', height:hp('8%'), width:'100%', 
//           alignItems: "center", marginEnd:'5%', }}>
//              <Image
//               style={{
//                 resizeMode: "contain",
//                 height:hp('5%'),
//                 width: hp('5%'),
//                marginStart:'5%'
//               }}
//               source={require("../image/user.png")}
//             />
//              <Text style={{ color: '#034d94', fontSize: 14, marginStart:'1%',width:'55%' }}> {this.state.userName}</Text>
//              {/* <Text style={{ color: 'red', fontSize: 16, fontWeight:'bold', alignItems:'flex-end' }}> Logout</Text> */}
//              <TouchableOpacity
//             onPress={() => { this.props.navigation.navigate('Login') }} >
//              <Text style={{fontSize:16, color:'green', fontWeight:'bold'}}>Login</Text>
//           </TouchableOpacity>
//         </View> 
//         <View style={{backgroundColor:'white',  height:hp('70%'), paddingTop: hp('6%')}}>

//         <TouchableOpacity
//             style={styles.listStyle}
//             onPress={() => { this.props.navigation.navigate('PendingPayment') }} >
//              <Text style={styles.listText}>Pending Payment</Text>
//           </TouchableOpacity>

//            <TouchableOpacity
//              style={styles.listStyle}
//            onPress={() => { this.props.navigation.navigate('TenantApplication') }} >
//             {/* <Image
//               style={{
//                 resizeMode: "contain",
//                 height: "100%",
//                 width: "10%",
//                 marginEnd: 10
//               }}
//               source={require("../image/dotImage.png")}
//             /> */}
//             <Text style={styles.listText}>Tenant Application</Text>
//           </TouchableOpacity>
//            <TouchableOpacity
//              style={styles.listStyle}
//             onPress={() => { this.props.navigation.navigate('PaymentHistory') }} >
//              <Text style={styles.listText}>Payment History</Text>
//           </TouchableOpacity>
//            <TouchableOpacity
//              style={styles.listStyle}
//             onPress={() => { this.props.navigation.navigate('MyProperties') }} >
//              <Text style={styles.listText}>My Properties</Text>
//           </TouchableOpacity>
//           <TouchableOpacity
//              style={styles.listStyle}
//             onPress={() => { this.props.navigation.navigate('MyCreditReport') }} >
//              <Text style={styles.listText}>My Credit Report</Text>
//           </TouchableOpacity>
//           <TouchableOpacity
//              style={styles.listStyle}
//             onPress={() => { this.props.navigation.navigate('MyTenants') }} >
//              <Text style={styles.listText}>My Tenants</Text>
//           </TouchableOpacity>
//           <TouchableOpacity
//              style={styles.listStyle}
//             onPress={() => { this.props.navigation.navigate('ChangePassword') }} >
//              <Text style={styles.listText}>Change Password</Text>
//           </TouchableOpacity>
//            <TouchableOpacity
//              style={styles.listStyle}
//             onPress={() => { this.props.navigation.navigate('PropertyInfo') }} >
//              <Text style={styles.listText}>Property Information</Text>
//           </TouchableOpacity>
//            <TouchableOpacity
//              style={[styles.listStyle,{marginStart:'70%'}]}
//             onPress={() => { this.props.navigation.navigate('Login') }} >
//              <Text style={{fontSize:15, color:'green', fontWeight:'bold'}}>Login</Text>
//           </TouchableOpacity>
          

//         </View>
//           </View>
//            </SafeAreaView>
//     );
//   }
// }