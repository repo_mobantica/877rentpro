import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import CheckBox from 'react-native-check-box';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ToolBarForProfile from '../component/ToolBarForProfile';
var radio_props = [
  {label: 'Landlord', value: 0 },
  {label: 'Tenant', value: 1 }
];
export default class MyProfile extends Component {
    constructor(props) {
    super(props);

    this.state = {
      tenant:false,
      manager:false,
      landlord:false,
      broker:false,
      firstName:'',
      middleName:'',
      lastName:'',
      address:'',
      email:'',
      message:'',
       mobileNo:'',
      country:'',
      state:'',
      city:'',
      birthday:'',
      income:'',
      userId:''
    };   
  }
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <ToolBarForProfile title="My Profile " />    
           <View style={styles.mainView}>
           <ScrollView >
           <View style={{paddingStart:'7%', paddingEnd:'7%'}}>
          <View style={styles.titleView}>
            <Text style={styles.titleText}>General Account Information</Text>
            </View>
             <View style={styles.textinputBorder}></View>

              <Text style={styles.nameStyle}>Email</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />

        <Text style={styles.nameStyle}>First Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(firstName) => this.setState({firstName})}
          value={this.state.firstName}
        />

        <Text style={styles.nameStyle}>Middle Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />

        <Text style={styles.nameStyle}>Last Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />

        <Text style={styles.nameStyle}>Country</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />

        <Text style={styles.nameStyle}>State</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />

        <Text style={styles.nameStyle}>City</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />
        
         <View style={styles.titleView}>
            <Text style={styles.titleText}>Personal Information</Text>
            </View>
             <View style={styles.textinputBorder}></View>

              <Text style={styles.nameStyle}>Birthday</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />

        <Text style={styles.nameStyle}>Annual Income(in $)</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(firstName) => this.setState({firstName})}
          value={this.state.firstName}
        />

        <Text style={styles.nameStyle}>Mobile Number</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />

         <Text style={styles.nameStyle}>User Type ID</Text>
          <View style={{height:hp('10%'), justifyContent:'center',  }}>
              <RadioForm
               radio_props={radio_props}
               initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({value:value})}}
                   />
             </View>


         </View>
       </ScrollView>
      <TouchableOpacity style={styles.lastButton}>
      <Text style={styles.sendText}> Update</Text>
     </TouchableOpacity>
     
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:'100%',
     width:'90%',
     margin:wp('5%'), 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1,
     
},

iconRow:{
  height:hp('5%'),
   marginTop:hp('1%'), 
    width:'100%',
     flexDirection:'row', 
     alignItems:'center',
      //backgroundColor:'red',
  
     },
    checkboxText:{
      width:'42%',
      fontSize:13,
       color:'gray'
    },
     titleView:{
         height:hp('6%'), 
         justifyContent:'center',
        marginTop:hp('2%')
      },
      titleText:{
        fontSize:16,
        // fontWeight:'bold'
    },
    nameStyle:{
    marginTop:hp('1%'), 
  // backgroundColor:'yellow', 
 },
 textInput:{
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
    backgroundColor:'gray',
     height:1,
      width:'100%',
      marginBottom:hp('3%')
      //marginTop:-hp('1%')
      },
   textInputView:{
           height:hp('13%'),
            marginStart:'10%',
             marginEnd:'10%',
             justifyContent: 'flex-start',
   },
   lastButton:{
     width:'100%',
       marginTop:hp('2%'),
        height:'8%', 
        borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
        backgroundColor:'#008f2c', 
        justifyContent:'center', 
        alignItems:'center',
       
   },
   sendText:{
     color:'white',
      fontWeight:'bold'
   },
  
});

