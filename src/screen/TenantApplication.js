import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
 import ToolbarHomeImg from '../component/ToolbarHomeImg'

export default class TenantApplication extends Component {
  _renderItem = ({item}) => (
    <View style={styles.flatListView}>
       <Image
          style={styles.displayImage}
          source={require('../image/room.png')}
       />
        <View style={styles.firstRow}>
    <Text style={styles.priceStyle}>$210</Text>
    <Image
          style={{width: 21, height:21}}
          source={require('../image/eye.png')}
       />
    </View>
    <Text style={styles.quantityText}>4 Bed | 3 Balcony | 3 Bathrooms</Text>
     <Text style={styles.addressText}>chicago, IL, USA</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 18, height:18}}
          source={require('../image/clock.png')}
       />
     <Text style={styles.applyText}>Applied, Yesterday at 12:09 PM</Text>
      {
        item === 0 ?
        <View style={styles.unpaidView  }> 
      <Text style={{fontSize:13}}>UNPAID</Text>
     </View>:
      <View style={styles.paidView}> 
      <Text style={{fontSize:13}}>PAID</Text>
     </View>
      }
     </View>
     {
       item === 0 ?
       <View style={styles.makePayment}>
     <Text style={{color:'white'}}> $ MAKE PAYMENT</Text>
     </View>
       :
       <View style={styles.acceptOrReject}>
    <View style={styles.acceptView}>
    <Text style={{color:'white'}}>ACCEPT</Text>
    </View>
    <View style={styles.rejectView}>
    <Text style={{color:'white'}}>REJECT</Text>
    </View>
     </View>
     }
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
     <ToolbarHomeImg title={"Tenant Application"} />
            <FlatList
        data={[0,1,1,0]}
      //  extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />

          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
  headerView: {
      position: 'absolute',
    flexDirection: "row",
    backgroundColor: "white",
    justifyContent: "space-between",
    //paddingHorizontal: 10,
    alignItems: "center",
    height:  hp('9%'),
    width:'100%'
  },
   flatListView:{
     height:hp('32%'),
     margin:hp('3%'),
     marginTop:hp('1.5%'),
     marginBottom:hp('1.5%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
    displayImage:{
       borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
        width: '100%', 
        height: hp('10%')
    },
    priceStyle:{
      color:'#034d94',
       fontWeight:'bold'
    },
    quantityText:{
      color:'#939393',
       marginStart:wp('3.7%'),
        fontSize:10
    },
   addressText:{
     color:'black', 
     marginStart:wp('3.7%'),
      fontWeight:'bold',
   },
   applyText:{
     color:'#939393', 
     marginStart:wp('1.6%'),
      fontSize:10
   },
    unpaidView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#ffcccd', 
        borderRadius:15, height:hp('3%'),
         borderColor:'#f0b0b2',
      borderWidth:2,
      marginStart:wp('19%'),
     width:65
    },
    paidView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#cbe8d8',
         borderRadius:15, 
         height:hp('3%'),
          borderColor:'green',
      borderWidth:2,
    marginStart:wp('19%'),
     width:65
    },
    makePayment:{
      justifyContent:'center',
       alignItems:'center',
        height:hp('6.5%'),
      backgroundColor:'#008f2c',
       borderBottomLeftRadius:11,
        borderBottomRightRadius:11
    },
    acceptOrReject:{
      height:hp('6.5%'),
       flexDirection:'row' 
    },
    acceptView:{
      borderBottomLeftRadius:11,
       width:'50%',
        backgroundColor:'#00508f',
         justifyContent:'center',
          alignItems:'center'
    },
    rejectView:{
       width:'50%',
        backgroundColor:'#eb3b48',
         borderLeftColor:'white',
          borderLeftWidth :1,
           justifyContent:'center',
            alignItems:'center',
        borderBottomRightRadius:11
    }

});
