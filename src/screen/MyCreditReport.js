import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
export default class MyCreditReport extends Component {
  _renderItem = ({item}) => (
    <View style={styles.flatListView}>
   <Image
          style={styles.displayImage}
          source={require('../image/room.png')}
       />
        <View style={styles.firstRow}>
    <Text style={styles.prizeText}>$ 1200 </Text>
    <Image
          style={styles.imageStyle}
          source={require('../image/eye.png')}
       />
    </View>

      <Text style={styles.location}>chicago, IL, USA</Text>
       <View style={styles.lastRow}>
       <View style={{flexDirection:'row', }}>
       <Image
           style={{width: hp('2%'), height:hp('2%'), marginEnd:wp('2%') }}
          source={require('../image/calendar.png')}
       />
    <Text >24th sep 1989 </Text>
    </View>
     <View style={styles.viewButton}> 
      <Text style={{fontSize:10}}>PENDING</Text>
     </View>
    </View>
     <View style={styles.viewStyle}>
     <Text style={{color:'white'}}> Download Reports</Text>
     </View>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
     <ToolbarHomeImg title="My Credit Reports" />

            <FlatList
        data={[1,2,3,4]}
      //  extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('29%'),
     marginStart:hp('3%'),
     marginEnd:hp('3%'),
     marginTop:hp('1.5%'),
     marginBottom:hp('1.5%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginEnd:wp('3.7%'),
   height:hp('4.5'),
   // backgroundColor:'red',
     alignItems:'center',
     marginBottom:hp('1%'),
     justifyContent: "space-between",
    },
   imageStyle:{ 
     width: hp('3%'), 
     height:hp('3%'),
      },
      viewStyle:{
        justifyContent:'center', 
        alignItems:'center', 
        height:hp('6.5%'),
      backgroundColor:'#034d94', 
      borderBottomLeftRadius:11,
       borderBottomRightRadius:11, 
      flexDirection:'row'
      },
      viewButton: {
        justifyContent:'center', 
        alignItems:'center',
         backgroundColor:'#d3eff5',
          borderRadius:15, 
          height:hp('3.5%'),
      borderColor:'#034d94',
       borderWidth:2,
         width:wp('20%')
         },
         displayImage:{
            borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
        width: '100%', 
        height: hp('10%')
        },
        location:{
          color:'black',
           marginStart:wp('3.7%'),
            fontWeight:'bold'
        },
        noOfItemtext:{
          color:'gray',
           marginStart:wp('3.7%'),
            fontSize:12
            },
            prizeText:{
              color:'#034d94',
               fontWeight:'bold'
               }
});