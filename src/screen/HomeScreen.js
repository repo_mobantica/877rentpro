// import React from 'react';
// import { StyleSheet, Text, View , TextInput} from 'react-native';
// import MapView, {Marker} from 'react-native-maps';

// export default class HomeScreen extends React.Component {
//   onRegionChange(region) {
//     this.setState({ region });
//   }
//   render() {
//     var mapStyle=[{"elementType": "geometry", "stylers": [{"color": "#242f3e"}]},{"elementType": "labels.text.fill","stylers": [{"color": "#746855"}]},{"elementType": "labels.text.stroke","stylers": [{"color": "#242f3e"}]},{"featureType": "administrative.locality","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "poi","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "poi.park","elementType": "geometry","stylers": [{"color": "#263c3f"}]},{"featureType": "poi.park","elementType": "labels.text.fill","stylers": [{"color": "#6b9a76"}]},{"featureType": "road","elementType": "geometry","stylers": [{"color": "#38414e"}]},{"featureType": "road","elementType": "geometry.stroke","stylers": [{"color": "#212a37"}]},{"featureType": "road","elementType": "labels.text.fill","stylers": [{"color": "#9ca5b3"}]},{"featureType": "road.highway","elementType": "geometry","stylers": [{"color": "#746855"}]},{"featureType": "road.highway","elementType": "geometry.stroke","stylers": [{"color": "#1f2835"}]},{"featureType": "road.highway","elementType": "labels.text.fill","stylers": [{"color": "#f3d19c"}]},{"featureType": "transit","elementType": "geometry","stylers": [{"color": "#2f3948"}]},{"featureType": "transit.station","elementType": "labels.text.fill","stylers": [{"color": "#d59563"}]},{"featureType": "water","elementType": "geometry","stylers": [{"color": "#17263c"}]},{"featureType": "water","elementType": "labels.text.fill","stylers": [{"color": "#515c6d"}]},{"featureType": "water","elementType": "labels.text.stroke","stylers": [{"color": "#17263c"}]}];
//     return (
//       <View style={styles.container}>
//         <MapView
//           style={styles.map}
//           initialRegion={{
//             latitude: 34.052235,
//             longitude: -118.243683,
//             latitudeDelta: 0.0922,
//             longitudeDelta: 0.0421,
//           }}
//          // customMapStyle={mapStyle}
//         >
//            {/* ['Los Angeles', 34.052235, -118.243683],
//     ['Santa Monica', 34.024212, -118.496475],
//     ['Redondo Beach', 33.849182, -118.388405],
//     ['Newport Beach', 33.628342, -117.927933],
//     ['Long Beach', 33.770050, -118.193739]
//     34.0522° N, 118.2437° W
//     */}
//           <Marker
//             draggable
//             coordinate={{
//               latitude: 34.052235,
//               longitude: -118.243683,
//             }}
//             onDragEnd={(e) => alert(JSON.stringify(e.nativeEvent.coordinate))}
//             title={'Test Marker'}
//             description={'This is a description of the marker'}
//           />
//            <Marker
//             draggable
//             coordinate={{
//               latitude:  33.849182,
//               longitude:-118.388405,
//             }}
//             onDragEnd={(e) => alert(JSON.stringify(e.nativeEvent.coordinate))}
//             title={'Test Marker'}
//             description={'This is a description of the marker'}
//           />
//           <Marker
//             draggable
//             coordinate={{
//               latitude:  33.628342,
//               longitude:-117.927933,
//             }}
//             onDragEnd={(e) => alert(JSON.stringify(e.nativeEvent.coordinate))}
//             title={'Test Marker'}
//             description={'This is a description of the marker'}
//           />
//         </MapView>
//       </View>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   container: {
//     position:'absolute',
//     top:0,
//     left:0,
//     right:0,
//     bottom:0,
//     alignItems: 'center',
//     justifyContent: 'flex-end',
//   },
//   map: {
//     position:'absolute',
//     top:0,
//     left:0,
//     right:0,
//     bottom:0,
//   },
// });
//-----------------------------------------------------------
import React from 'react';
import { StyleSheet, Text, View, TextInput, Dimensions, Alert} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import Geocoder from 'react-native-geocoder';
//import { SearchBar } from 'react-native-elements';
import { Searchbar } from 'react-native-paper';

export default class HomeScreen extends React.Component {
 state = {
   focusedLocation: {
     latitude: 34.052235,
     longitude: -118.243683,
     latitudeDelta: 0.0122,
     longitudeDelta:Dimensions.get("window").width / Dimensions.get("window").height*0.0122
   },
   locationChoosen:false,
   search: '',  
 }

 picKLocationHandler = event =>{
   const coords = event.nativeEvent.coordinate;
   this.map.animateToRegion({
     ...this.state.focusedLocation,
     latitude: coords.latitude,
     longitude: coords.longitude
   });
   this.setState(prevState => {
      return{
        focusedLocation: {
          ...prevState.focusedLocation,
          latitude: coords.latitude,
          longitude:coords.longitude
        },
        locationChoosen:true
      };
   });
 }
 //'lat',res.position[lat)
 updateSearch = search => {
  this.setState({ search });
};
 searchLocation(){
      Geocoder.geocodeAddress(this.state.search).then(res => {
        // console.log('aaaaaaaaa');
        // console.log(res );
        // console.log('aaaaaaaaa');
        // console.log('lng',res[0].position.lng,'lat',res[0].position.lat );
        // console.log('aaaaaaaaa');
       
        this.map.animateToRegion({
          ...this.state.focusedLocation,
          latitude: res[0].position.lat,
          longitude:res[0].position.lng
        });
        this.setState(prevState => {
           return{
             focusedLocation: {
               ...prevState.focusedLocation,
               latitude: res[0].position.lat,
               longitude:res[0].position.lng
             },
             locationChoosen:false
           };
        });
      })
      .catch(err => console.log(err))
    }
 render() {
 
    //  let marker = null;
      const { search } = this.state;
    //  if(this.state.locationChoosen){
    //    marker = <MapView.Marker coordinate ={this.state.focusedLocation} />
    //  }
   return (
     <View style={styles.container}>
       <Searchbar
        placeholder="Type Here..."
      //  onChangeText={this.searchLocation()}
        onChangeText={query => { this.setState({ search: query }, function(){
          this.searchLocation();
        }); }}
        value={this.state.search}
        
       // style={{width:300}}
      /> 
       <MapView
         style={styles.map}
         initialRegion={this.state.focusedLocation}
       //  region ={this.state.focusedLocation}
         // onPress ={this.searchLocation()}
           onPress ={this.picKLocationHandler}

          ref = {ref => this.map =ref  }
          >
        {marker}
        {/* {this.state.locationChoosen ?
        <View>
        <MapView.Marker  coordinate ={this.state.focusedLocation}>
  <View style={styles.circle}>
     <Text style={styles.pinText}>1</Text>
   </View></MapView.Marker>
   </View>
   :null}  */}
        
       </MapView>
     
     </View>
   );
 }
}

const styles = StyleSheet.create({
 container: {
   position: 'absolute',
   top: 0,
   left: 0,
   right: 0,
   bottom: 0,
   alignItems: 'center',
   justifyContent: 'flex-end',
 },
 map: {
   position: 'absolute',
   top: 0,
   left: 0,
   right: 0,
   bottom: 0,
  height:'90%'
 },
 circle: {
    width: 30,
    height: 30,
    borderRadius: 30 / 2,
    backgroundColor: 'red',
},
pinText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
},
});



  
  
