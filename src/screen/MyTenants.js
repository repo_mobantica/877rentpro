import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';

export default class MyTenants extends Component {
  _renderItem = ({item}) => (
    <View style={styles.flatListView}>
   <Image
          style={styles.displayImage}
          source={require('../image/room.png')}
       />
        
    <Text style={styles.userName}>James</Text>
    
     <Text style={styles.address}>chicago, IL, USA</Text>
     <View style={styles.dateRow}>
       <Image
           style={styles.icon}
          source={require('../image/calendar.png')}
       />
    <Text style={styles.dateText}>Available From, September 01 </Text>
    </View>
     

      <View style={styles.lastRow}>
       <View style={styles.phoneView}>
     <Image
          style={styles.icon}
          source={require('../image/call.png')}
       />
       <Text style={{color:'white'}}> Phone</Text>
       </View>
       <View style={styles.middleView}>
       <Image
          style={styles.icon}
          source={require('../image/mail.png')}
       />
       <Text style={{color:'white'}}> Email</Text>
       </View>
        <View  style={styles.phoneView}>
     <Image
          style={styles.icon}
          source={require('../image/comment.png')}
       />
       <Text style={{color:'white'}}> Message</Text>
       </View>

       </View>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={[styles.container,{backgroundColor:'red'}]}>
      <View style={styles.container}>
      <ToolbarWithLeftIcon title="My Tenants" />
             
       <FlatList
        data={[1,2,3,4]}
        extraData={this.state}
      keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
   <View style={{height:hp('1%'), width:'100%'}}></View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
   flatListView:{
     height:hp('30%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
     dateRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   height:hp('4'),
   // backgroundColor:'red',
     alignItems:'center',
     marginBottom:hp('1%')
    },
    lastRow:{
      flexDirection:'row', 
     borderBottomRightRadius: 11,
     backgroundColor:'#034d94',
    alignItems:'center',
    flex:1,
    height:hp('6%'),
    borderBottomLeftRadius: 11,
    },
    displayImage:{
      width: '100%',
      height: hp('10%'), 
     borderTopLeftRadius: 11, 
      borderTopRightRadius: 11,
     },
     userName:
     {color:'#034d94',
      marginStart:wp('3.7%'), 
      marginTop:wp('3.7%'), 
      fontWeight:'bold'
      },
    address:{
      color:'black',
       marginStart:wp('3.7%'),
        fontWeight:'bold'
    },
    phoneView:{
      flexDirection:'row', 
      width:'33%', 
      height:"100%",
       justifyContent: "center",
     alignItems:'center',
     },
     middleView:{
       flexDirection:'row',
        width:'33%',
         height:"100%",
        borderLeftWidth:1,
       borderColor:'white',
      borderRightWidth:1,
         justifyContent: "center",
         alignItems:'center',
      },
         icon:{
           height:hp('3%'),
           width:hp('3%'),
           marginEnd:wp('2%')
       },
       dateText:{
         marginStart:wp('2%'),
          color:'gray', 
         fontSize:12
         }
});