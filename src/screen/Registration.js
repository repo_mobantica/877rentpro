import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList, TextInput, ScrollView } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
import RNPickerSelect from 'react-native-picker-select';
import DateTimePicker from "react-native-modal-datetime-picker";
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: 'param1', value: 0 },
  {label: 'param2', value: 1 }
];
export default class Registation extends Component {
     
      constructor(props) {
          super(props);
         this.state = {
         firstName:'',
      lastName:'',
      middleName:'',
      email:'',
      password:'',
      confirmPass:'',
      income:'',
      Birthday:'09/09/2019',
      mobileNo:'',
      country:'',
      state:'',
      city:'',
      newPass:'',
      isDateTimePickerVisible: false,
       value: 0,
    };
  }

  showDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: true });
  };
 
  hideDateTimePicker = () => {
    this.setState({ isDateTimePickerVisible: false });
  };
 
  handleDatePicked = date => {
    console.log("A date has been picked: ", date);
     let day =date.getDate();
    let month =date.getMonth();
    let year =date.getFullYear();
     let fullDate = day + '/' +month +'/' +year;
     console.log("A date has been picked: ", fullDate);
    this.setState({ Birthday: fullDate });
    this.hideDateTimePicker();
   // Alert.alert(date)
  };
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
           <ToolbarHomeImg title="Sign Up" />    
           <View style={styles.mainView}>
           <View style={{ height:hp('72%'),}}>
            <ScrollView>
            <View style={styles.titleView}>
            <Text style={styles.titleText}>General Account Information</Text>
            </View>
            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Email</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Email"
          onChangeText={(email) => this.setState({email})}
          value={this.state.email}
        />
        <View style={styles.textinputBorder}></View>

       <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>First Name</Text>
             </View>
            <TextInput
         style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter First Name"
          onChangeText={(firstName) => this.setState({firstName})}
          value={this.state.firstName}
        />
        <View style={styles.textinputBorder}></View>

        <Text style={styles.nameStyle}>Middle Name</Text>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Middle Name"
          onChangeText={(middleName) => this.setState({middleName})}
          value={this.state.middleName}
        />
        <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Last Name</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Last Name"
          onChangeText={(lastName) => this.setState({lastName})}
          value={this.state.lastName}
        />
        <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Country</Text>
             </View>
             <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
                { label: 'Football', value: 'football' },
                { label: 'Baseball', value: 'baseball' },
                { label: 'Hockey', value: 'hockey' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>

         <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>State</Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
                { label: 'Football', value: 'football' },
                { label: 'Baseball', value: 'baseball' },
                { label: 'Hockey', value: 'hockey' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>
        
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>City</Text>
             </View>
        <View style={styles.textInput}>
             <RNPickerSelect
            onValueChange={(value) => console.log(value)}
            items={[
                { label: 'Football', value: 'football' },
                { label: 'Baseball', value: 'baseball' },
                { label: 'Hockey', value: 'hockey' },
            ]}
        />
        </View>
        <View style={styles.textinputBorder}></View>

         <View style={{height:hp('10%'), justifyContent:'center', alignItems:'center',}}>
            <Text style={{fontSize:16, fontWeight:'bold'}}>Personal Information</Text>
            </View>

            <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Birthday</Text>
             </View>
             <View style={styles.birthdayView}>
             
              <Text>{this.state.Birthday}</Text>
            
        <TouchableOpacity 
        style={styles.calenderIconView}
       onPress={this.showDateTimePicker}>
         <Image style={styles.calenderImg} source={require('../image/calendar.png')}/>
        </TouchableOpacity>
        </View>
        <View style={styles.textinputBorder}></View>
        
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Annual Income</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Annual Income"
          onChangeText={(income) => this.setState({income})}
          value={this.state.income}
        />
        <View style={styles.textinputBorder}></View>
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Mobile Number</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter your Mobile Number"
          onChangeText={(mobileNo) => this.setState({mobileNo})}
          value={this.state.mobileNo}
        />
        <View style={styles.textinputBorder}></View>

        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>User Type ID</Text>
             </View>
             <View style={{height:hp('10%'), justifyContent:'center', paddingStart:wp('10%') }}>
              <RadioForm
               radio_props={radio_props}
               initial={0}
                 formHorizontal={true}
                 buttonSize={15}
                 buttonInnerColor= {'#034d94'}
                 buttonOuterColor ={'#034d94'}
                 labelStyle={{marginRight:wp('15%'),}}
                         onPress={(value) => {this.setState({value:value})}}
                   />
             </View>
           
        <View style={styles.textinputBorder}></View>
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Password</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Password"
          onChangeText={(password) => this.setState({password})}
          value={this.state.password}
        />
        <View style={styles.textinputBorder}></View>
        <View style={styles.requiredField}>
            <View style={styles.requiredDot}></View>
             <Text style={styles.nameStyleWithDot}>Confirm Password</Text>
             </View>
            <TextInput
          style={styles.textInput}
           underlineColorAndroid = "transparent"
          placeholder="Enter Confirm Password"
          onChangeText={(confirmPass) => this.setState({confirmPass})}
          value={this.state.confirmPass}
        />
        <View style={[styles.textinputBorder,{marginBottom:hp('3%')}]}></View>
         </ScrollView>
        </View>
       
         <DateTimePicker
          isVisible={this.state.isDateTimePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.hideDateTimePicker}
        />  
      
        <TouchableOpacity style={styles.buttonStyle}>
        <Text style={styles.buttonText}>Sign Up</Text>
        </TouchableOpacity>
        </View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  mainView:{
    height:hp('80%'),
     width:'86%',
     margin:'7%', 
     backgroundColor:'white',
      borderRadius:10, 
      flex:1},
 nameStyle:{
    marginTop:hp('2%'), 
     marginStart:wp('10%'),
  // backgroundColor:'yellow', 
 },
 nameStyleWithDot:{
     marginTop:hp('2%'), 
     marginStart:wp('2%'),
 },
 textInput:{
    marginStart:wp('10%'), 
     marginTop:-hp('1.3%'),
     //backgroundColor:'yellow', 
    // height:hp('5%')
 },
 textinputBorder:{
   marginStart:wp('5%'), 
   marginEnd:wp('5%'), 
    backgroundColor:'gray',
     height:1,
      width:'85%',
      marginTop:-hp('1%')
      },
  buttonStyle:{height:hp('8%'),
       width:'100%',
       borderBottomLeftRadius:10,
       borderBottomRightRadius:10, 
       backgroundColor:'#008f2c',
        // marginTop:hp('24%'),
        //justifyContent:'flex-end',
     alignItems:'center'
  }, 
  requiredDot:{
    height:6,
     width:6,
      borderRadius:3,
       backgroundColor:'red',
         marginStart:wp('8%'),
         marginTop:hp('2%'), 
       },
       titleView:{
         height:hp('8%'), 
         justifyContent:'center',
          alignItems:'center'
      },
      titleText:{
        fontSize:16,
         fontWeight:'bold'
    },
    requiredField:{
      flexDirection:'row',
       alignItems:'center'
    },
    buttonText:{
      fontWeight:'bold', 
      fontSize:16, 
      color:'white', 
      marginTop:hp('1.3%')
      },
      calenderIconView:{
        width:wp('20%'), 
      marginTop:-hp('1%'),
       justifyContent:'center',
       alignItems:'center'
      },
       calenderImg:{
         height:hp('3%'),
       width:hp('3%')
      },
      birthdayView:{ 
        flexDirection:'row', 
        height:hp('6%'),
       marginStart:wp('10%'),
        justifyContent:'space-between',
        alignItems:'center',
        marginEnd:wp('5%')
       },
});

