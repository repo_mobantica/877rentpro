import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarHomeImg from '../component/ToolbarHomeImg';
export default class PaymentHistory extends Component {
  _renderItem = ({item}) => (
    <View style={styles.flatListView}>
   <Image
          style={{width: '100%', height: hp('10%'),  borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,}}
          source={require('../image/room.png')}
       />
        <View style={styles.firstRow}>
    <Text style={{color:'#034d94', fontWeight:'bold'}}>$210</Text>
    <Image
          style={{width: 21, height:21}}
          source={require('../image/eye.png')}
       />
    </View>
     <Text style={{color:'black', marginStart:wp('3.7%'), fontWeight:'bold',}}>chicago, IL, USA</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 10.5, height:10.5}}
          source={require('../image/calendar.png')}
       />
     <Text style={{color:'#939393', marginStart:wp('1.6%'), fontSize:10 }}>August, 09 2019</Text>
     <View style={{justifyContent:'center', alignItems:'center', backgroundColor:'#cbe8d8', borderRadius:15, height:hp('3%'), borderColor:'green',
   borderWidth:2, marginStart:wp('40%'), width:65}}> 
      <Text style={{fontSize:13}}>PAID</Text>
     </View>
     </View>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
     <ToolbarHomeImg title="Payment History" />
            <FlatList
        data={[1,2,3,4]}
      //  extraData={this.state}
       keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />

          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('24%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:11,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
    
});