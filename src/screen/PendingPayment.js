import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
 import ToolbarHomeImg from '../component/ToolbarHomeImg';
export default class PendingPayment extends Component {
  _renderItem = ({item}) => (
    <View style={styles.flatListView}>
   <Image
          style={styles.displayImg}
          source={require('../image/room.png')}
       />
        <View style={styles.firstRow}>
    <Text style={styles.priceText}>$210</Text>
    <Image
          style={styles.eyeIcon}
          source={require('../image/eye.png')}
       />
    </View>
     <Text style={styles.addressText}>chicago, IL, USA</Text>
     <View style={styles.lastRow}>
     <Image
          style={styles.calenderImg}
          source={require('../image/calendar.png')}
       />
     <Text style={styles.dueDateText}>Due on: Today</Text>
      <View style={styles.rentView}> 
      <Text style={{fontSize:10}}>RENT</Text>
     </View>
     </View>
     <View style={styles.makePayment}>
     <Text style={{color:'white'}}> $ MAKE PAYMENT</Text>
     </View>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
      <ToolbarHomeImg title="Pending Payment" />
            
            <FlatList
        data={[1,2,3,4]}
       extraData={this.state}
       keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
        <View style ={{height:hp('3%'), width:'100%'}}></View>
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('29%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:'5%',
   marginTop:wp('3.7%'),
    marginEnd:'5%',
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:'5%',
   marginTop:-6,
    marginEnd:'5%',
   alignItems:'center',
   flex:1
    },
    makePayment:{
      justifyContent:'center',
       alignItems:'center',
        height:hp('6.5%'),
      backgroundColor:'#008f2c',
       borderBottomLeftRadius:11,
        borderBottomRightRadius:11
    },
    rentView:{
      justifyContent:'center',
       alignItems:'center',
        backgroundColor:'#e7e7e7',
         borderRadius:15,
          height:hp('3%'), 
          borderColor:'black',
   borderWidth:1,
    marginStart:'45%',
     width:65
    },
    dueDateText:{
      color:'gray',
       marginStart:wp('1.6%'),
        fontSize:12 
    },
    calenderImg:{
      width: 11, height:11
    },
    addressText:{
      color:'black',
       marginStart:'5%', 
       fontWeight:'bold',
    },
    displayImg:{
       borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,
        width: '100%',
         height: hp('10%')
    },
    eyeIcon:{
      width: 21, height:21, 
    },
    priceText:{
      color:'#034d94', fontWeight:'bold'
    }
});