import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, TextInput } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Slider from '@react-native-community/slider';
import dotImage from '../image/dotImage.png'
import ToggleSwitch from 'toggle-switch-react-native'
import { Switch } from 'react-native-switch';
import CheckBox from 'react-native-check-box'
import { ScrollView } from 'react-native-gesture-handler';
import { config } from "../config/index";
import SwitchText from "../component/SwitchText";
import ActiveSwitchImg from "../component/ActiveSwitchImg";
import RangeSlider from 'react-native-range-slider'
import ToolbarForFilter from '../component/ToolbarForFilter'
export default class PropertyInfo extends React.Component {
 constructor(props) {
   super(props);
   this.state = {
     //Initial Value of slider
     sliderValue: 15,
      sliderValue1: 15,
      firstToggle:false,
      secondToggle:false,
      tab:'Housing',
      isApartment:false,
      isCombo:false,
      isTownHome:false,
      isRoom:false,
      isHouse:false,
      isDuplex:false,
      isSingleFam:false,
      keyword:'',
      beds:3,
      bathrooms:3
   };
 }
  tabPress(tabPass){
   this.setState({
            tab:tabPass
          })
        // Alert.alert(tabPass);
  }
 
 onBedPress(item){
   this.setState({
     beds:item
   },function(){
     console.log('no. of. bed');
     console.log(this.state.beds);
   })
 }

 onBathroomPress(item){
   this.setState({
       bathrooms:item
   })
 }

  render() {
    //Image Slider
    return (
        <View style={styles.container}> 
       <ToolbarForFilter image title={"Dashboard"} />
           <View style={styles.mainView}>
        
           <ScrollView
                style={{  paddingStart:'6%',
     paddingEnd:'6%', }}
                >  
                <View style={{justifyContent:'center', height:'5%', }}>
                <Text style ={{color:'black', fontSize: 13,}}>Price</Text>
         </View>
         <View style ={styles.priceLable}>
         <Text style={styles.sliderValue}>$0</Text>
         <Text style={styles.sliderValue}>$3000</Text>
         </View>
          <View style={styles.slider}>
         <Slider 
          maximumValue={100}
          minimumValue={0}
          minimumTrackTintColor="#034d94"
          maximumTrackTintColor="#000000"
          step={1} 
          value={this.state.sliderValue}
          onValueChange={(Value1) => this.setState({ Value1 })}
        />
         {/* <RangeSlider
    minValue={0}
    maxValue={100}
    tintColor={'#da0f22'}
    handleBorderWidth={1}
    handleBorderColor="#454d55"
    selectedMinimum={20}
    selectedMaximum={40}
    style={{ flex: 1, height: 70, padding: 10, backgroundColor: '#ddd' }}
    onChange={ (data)=>{ console.log(data);} }
  /> */}

       </View>
        <Text style ={{color:'black', fontSize: 13,marginTop:'8%' }}>SQFT</Text>
       <View style ={[styles.priceLable,{marginTop:'2%'}]}>
         <Text style={styles.sliderValue}>0</Text>
         <Text style={styles.sliderValue}>2000</Text>
         </View>
          <View style={styles.slider}>
         <Slider 
          maximumValue={100}
          minimumValue={0}
          minimumTrackTintColor="#034d94"
          maximumTrackTintColor="#000000"
          trackImage={require("../image/dotImage.png")}
         // trackImage={require('./slider.png')}
          step={1} 
          value={this.state.sliderValue1}
          onValueChange={(Value2) => this.setState({ Value2 })}
        />
       </View>
       <Text style ={{color:'black', fontSize: 12, marginTop:'8%'}}>Beds</Text>
       <View style = {styles.circleView}>
       <TouchableOpacity style={ this.state.beds ===1 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBedPress(1)}}>
          <Text style={this.state.beds ===1 ? styles.whiteText :null}> 1</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ this.state.beds ===2 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBedPress(2)}}>
          <Text style={this.state.beds ===2 ? styles.whiteText :null}> 2</Text>
        </TouchableOpacity>
           <TouchableOpacity style={ this.state.beds ===3 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBedPress(3)}}>
          <Text style={this.state.beds ===3 ? styles.whiteText :null}> 3</Text>
        </TouchableOpacity>
         <TouchableOpacity style={ this.state.beds ===4 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBedPress(4)}}>
          <Text style={this.state.beds ===4 ? styles.whiteText :null}> 4</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ this.state.beds ===5 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBedPress(5)}}>
          <Text style={this.state.beds ===5 ? styles.whiteText :null}> +4</Text>
        </TouchableOpacity>
       </View>

       <Text style ={{color:'black', fontSize: 12, marginTop:hp('2%'),}}>Bathrooms</Text>
       <View style = {styles.circleView}>
       <TouchableOpacity style={ this.state.bathrooms ===1 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBathroomPress(1)}}>
          <Text style={this.state.bathrooms ===1 ? styles.whiteText :null}> 1</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ this.state.bathrooms ===2 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBathroomPress(2)}}>
          <Text style={this.state.bathrooms ===2 ? styles.whiteText :null}> 2</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ this.state.bathrooms ===3 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBathroomPress(3)}}>
          <Text style={this.state.bathrooms ===3 ? styles.whiteText :null}> 3</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ this.state.bathrooms ===4 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBathroomPress(4)}}>
          <Text style={this.state.bathrooms ===4 ? styles.whiteText :null}> 4</Text>
        </TouchableOpacity>
        <TouchableOpacity style={ this.state.bathrooms ===5 ?styles.outerCircleSelected :styles.outerCircle}
       onPress={(e) => {e.stopPropagation(); this.onBathroomPress(5)}}>
          <Text style={this.state.bathrooms ===5 ? styles.whiteText :null}>+4</Text>
        </TouchableOpacity>
       </View>
         <Text style ={{color:'black', fontSize: 12, marginTop:hp('2%'), }}>Pet Friendly</Text>
         <View style={{flexDirection:'row', height:hp('5.6%'),  justifyContent:'space-between', marginTop:hp('1.5%'), 
        width:'100%',}}>
          
           <View style={styles.tabView}>
         <Text style={{marginStart:10}}>Dogs</Text>
         <Switch
       value={this.state.firstToggle}
       onValueChange={firstToggle => {
            this.setState({ firstToggle }); }}
      disabled={false}
     activeText={'On'}
        inActiveText={'Off'}
      backgroundActive={'green'}
      backgroundInactive={'white'}
      circleBorderColor={'gray'}
       circleBorderWidth={0.2}
        renderInsideCircle={() => this.state.firstToggle ? <ActiveSwitchImg/> :<SwitchText/>}
       />
    </View>

          <View style={styles.tabView1}>
         <Text style={{marginStart:10}}>Cats</Text>
         <Switch
           value={this.state.secondToggle}
            onValueChange={secondToggle => {
            this.setState({ secondToggle }); }}
            disabled={false}
            backgroundActive={'green'}
           backgroundInactive={'white'}
           circleBorderColor={'gray'}
           activeText={'1'}
            inActiveText={'2'}
             circleBorderWidth={0.2}
           renderInsideCircle={() => this.state.secondToggle ? <ActiveSwitchImg/> :<SwitchText/>}
    />
      </View>
           </View>
           <Text style ={{color:'black', fontSize: 12, marginTop:hp('3%'), }}>Property Type</Text>
           <View style={styles.mainTabView}>

          <TouchableOpacity style={{width:'50%', justifyContent:'center', alignItems:'center'}}
           onPress={(e) => {e.stopPropagation(); this.tabPress('Housing')}}
          >
          <Text>Housing</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{width:'50%', justifyContent:'center', alignItems:'center'}}
           onPress={(e) => {e.stopPropagation(); this.tabPress('Commercial')}}
           >
          <Text>Commercial</Text>
          </TouchableOpacity>
           </View>
           <View style={{ backgroundColor:'gray', height:1, width:'100%'}}>
           {
             this.state.tab == 'Housing'?
             <View style={styles.activeTabHouse}></View>
             : <View style={styles.activeTabCom}></View>
           }
           
           </View>
           {
             this.state.tab == 'Housing'?
              <View style={styles.tabContent}>
           <View style={{width:'50%', flexDirection:'column'}}>
           <View style={styles.innerContent}>
           <CheckBox
    style={{ padding: 10}}
    onClick={()=>{
      this.setState({
          isApartment:!this.state.isApartment
      })
    }}
    isChecked={this.state.isApartment}
    />
    <Text> Apartment</Text>
    </View>
    <View style={styles.innerContent}>
           <CheckBox
    style={{ padding: 10}}
    onClick={()=>{
      this.setState({
          isCombo:!this.state.isCombo
      })
    }}
    isChecked={this.state.isCombo}
    />
    <Text> Combo</Text>
     </View>
    <View style={styles.innerContent}>
           <CheckBox
    style={{ padding: 10}}
    onClick={()=>{
      this.setState({
          isTownHome:!this.state.isTownHome
      })
    }}
    isChecked={this.state.isTownHome}
    />
    <Text> TownHome</Text>
     </View>
    <View style={styles.innerContent}>
           <CheckBox
    style={{ padding: 10}}
    onClick={()=>{
      this.setState({
          isRoom:!this.state.isRoom
      })
    }}
    isChecked={this.state.isRoom}
    />
    <Text> Room Mate</Text>
           </View>
           
           </View>
           <View style={{width:'50%',flexDirection:'column', }}>
           <View style={styles.innerContent}>
           <CheckBox
        style={{ padding: 10}}
         onClick={()=>{
         this.setState({
             isHouse:!this.state.isHouse
          })
        }}
       isChecked={this.state.isHouse}
        />
       <Text> House</Text>
           </View>
          <View style={styles.innerContent}>
           <CheckBox
        style={{ padding: 10}}
         onClick={()=>{
         this.setState({
             isDuplex:!this.state.isDuplex
          })
        }}
       isChecked={this.state.isDuplex}
        />
       <Text> Duplex</Text>
           </View>
           <View style={styles.innerContent}>
           <CheckBox
        style={{ padding: 10}}
         onClick={()=>{
         this.setState({
             isSingleFam:!this.state.isSingleFam
          })
        }}
       isChecked={this.state.isSingleFam}
        />
       <Text> Singlefam</Text>
           </View>
           
           </View>
           </View>:null
           }
           <Text style ={styles.keywordText}>Keywords</Text>
           <View style={ styles.keywordTextInut}>
           <TextInput
          style={{height:hp('8%'), borderBottomWidth:1, borderColor:'black', }}
          placeholder="Keyword inserted appear here"
           underlineColor='gray'
          placeholderTextColor='gray'
          onChangeText={(text) => this.setState({text})}
          value={this.state.keyword}
        />
        <View style={{ marginBottom:hp('2%'), backgroundColor:'black', height:1, width:'100%'}}></View>
        </View>
            </ScrollView>
      
          <View style={styles.offerOuterView}>
                  <View style={styles.rentApplyView}>
                <Text style={styles.whiteText}>Apply for Rent</Text>
               </View> 
               <View style={styles.makeOfferView}>
              <Text style={styles.whiteText}>Make an Offer</Text>
               </View> 
               </View>
        </View> 
      </View>
    );
  }
}

const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
     mainView:{
    height:'85%',
     width:'90%',
     margin:wp('5%'), 
     backgroundColor:'white',
   // backgroundColor:'#f1f1f1',
      borderRadius:10, 
     // flex:1
},
  whiteText:{
      color:'white'
    },
rentApplyView:{
  width:'50%',
   backgroundColor:'#034d94', 
  borderBottomLeftRadius: 11,
   borderEndWidth:1,
   justifyContent:'center', 
  alignItems:'center',
   borderColor:'white'
},
makeOfferView:{
  justifyContent:'center',
   alignItems:'center',
  borderBottomRightRadius: 11,
   width:'50%',
    backgroundColor:'#034d94',
     borderColor:'white'
},
offerOuterView:{
  height:'9%', 
  marginTop:hp('2.3%'),
   flexDirection:'row'
}, 
  priceLable:{
      justifyContent: "space-between",
    flexDirection: "row",
    height:'3.5%',
    marginStart:'5%',
    width:'90%',
    marginBottom:'-2%'
  },
  slider:{
    height:hp('3%'),
   width:wp('90%'),
   // backgroundColor:'yellow',
    
  },
  outerCircleSelected: {
    borderRadius: 17,
    width: 34,
    height:34,
    backgroundColor: '#034d94',
    justifyContent:'center',
    alignItems:'center',
     elevation: 8
  },
  outerCircle: {
    borderRadius: 17,
    width: 34,
    height:34,
    backgroundColor: 'white',
    justifyContent:'center',
    alignItems:'center',
    shadowColor: 'gray',
shadowOffset: { width: 0, height: 2 },
shadowOpacity: 0.5,
shadowRadius: 2,
elevation: 2,
  },
  circleView: {
    height:hp('7%'),
    width:'100%',
    flexDirection:'row',
     justifyContent: "space-between",
     alignItems:'center',
    
  },
  sliderValue:{
    fontSize:10
  },
  tabView : {
    flexDirection:'row',
    height: hp('5.6%'),
   // marginStart:wp('6%'),
    justifyContent: "space-between",
    backgroundColor: '#dcdcdc',
    borderRadius:35,
    alignItems:'center',
   // flexDirection:'row',
   width:wp('33%')
  },
   tabView1 : {
    flexDirection:'row',
    height: hp('5.6%'),
  // marginStart:wp('60%'),
 //  marginBottom:hp('5%'),
 // marginTop:hp('62.5%'),
    justifyContent: "space-between",
    backgroundColor: '#dcdcdc',
    borderRadius:35,
    alignItems:'center',
   // flexDirection:'row'
   width:wp('33%')
  },
  mainTabView:{
 height:hp('6%'),
   flexDirection:'row',
 // backgroundColor:'red',
 },
 activeTabHouse:{
   width:'50%',
   backgroundColor:'#034d94',
   height:2
 },
 activeTabCom:{
   width:'50%',
   backgroundColor:'#034d94',
   height:2,
   marginStart:'50%'
 },
 tabContent:{
   height:'18%',
  // backgroundColor:'red',
   width:'100%',
   flexDirection:'row',
   marginTop:hp('1%')
 },
 innerContent:{
   flexDirection:'row', height:hp('4%'),  alignItems:'center'
 },
  saveSearch : {
    flexDirection:'row',
    position: 'absolute',
    height: hp('7%'),
    marginStart:wp('15%'),
 //  marginBottom:hp('5%'),
  marginTop:hp('87%'),
    justifyContent: "space-between",
    backgroundColor: 'white',
    borderRadius:50,
    alignItems:'center',
   // flexDirection:'row'
   width:wp('70%'),
   justifyContent:'center',
   alignItems:'center',
   borderColor:'black',
   borderWidth:0.5
  },
  keywordText:{
    color:'black',
     fontSize: 12,
      marginTop:hp('2%'),
  },
  keywordTextInut:{
    height:hp('14%'),
     borderBottomWidth:1,
      borderColor:'black', 
  },
  textInputText:{
    height:hp('14%'),
     borderBottomWidth:1, borderColor:'black', 
  }
});

