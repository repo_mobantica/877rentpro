import React, { Component } from 'react';
import { Text, View, SafeAreaView, Image, TouchableOpacity, StyleSheet, Dimensions, Alert,
Button, FlatList } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import IconBadge from "react-native-icon-badge";
import ToolbarWithLeftIcon from '../component/ToolbarWithLeftIcon';
export default class OutwardPendingPayment extends Component {
  _renderItem = ({item}) => (
    <View style={styles.flatListView}>
   <Image
          style={{ borderTopLeftRadius: 11, 
        borderTopRightRadius: 11,width: '100%', height: hp('10%')}}
          source={require('../image/room.png')}
       />
        <View style={styles.firstRow}>
    <Text style={{color:'#034d94', fontWeight:'bold'}}>$210</Text>
    
    <Image
          style={{width: 21, height:21, }}
          source={require('../image/eye.png')}
       />
    </View>
     <Text style={{color:'black', marginStart:wp('3.7%'), fontWeight:'bold',}}>chicago, IL, USA</Text>
     <View style={styles.lastRow}>
     <Image
          style={{width: 11, height:11}}
          source={require('../image/calendar.png')}
       />
     <Text style={{color:'gray', marginStart:wp('1.6%'), fontSize:10 }}>Due on: Today</Text>
     
     </View>
     <View style={{justifyContent:'center', alignItems:'center', height:hp('6.5%'),
      backgroundColor:'#008f2c', borderBottomLeftRadius:11, borderBottomRightRadius:11}}>
     <Text style={{color:'white'}}> $ MAKE PAYMENT</Text>
     </View>
    </View>
  );
  render() {
    
    return (
       <SafeAreaView style={styles.container}>
      <View style={styles.container}>
       <ToolbarWithLeftIcon title="Outward Pending Payment" />
            {/* <View style={{marginTop:hp('9%')}}> */}
            <FlatList
        data={[1,2,3,4]}
      //  extraData={this.state}
       keyExtractor={(item, index) => index.toString()}
        renderItem={this._renderItem}
      />
{/* </View> */}
          </View>
           </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
container: {
    flex: 1,
   backgroundColor: "#ededed"
  },
  menuIcon: {
    height: "40%",
    resizeMode: "contain",
    width: 30,
    marginStart: 5
  },
   flatListView:{
     height:hp('29%'),
     margin:hp('3%'),
     marginTop:hp('3%'),
     marginBottom:hp('0%'),
     borderRadius:13,
     backgroundColor:'white',
       
  },
  firstRow:{
    flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:wp('3.7%'),
    marginEnd:wp('3.7%'),
    justifyContent: "space-between",
    },
    lastRow:{
      flexDirection:'row', 
   marginStart:wp('3.7%'),
   marginTop:-6,
    marginEnd:wp('3.7%'),
   alignItems:'center',
   flex:1
    },
    
});