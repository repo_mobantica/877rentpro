import React from "react";
import {
 Text,
 View,
 AsyncStorage,
 Image,
 StyleSheet,
 TouchableOpacity,
 Platform,
 Alert,
 Button,
 SafeAreaView, Dimensions
} from "react-native";
import {
 createBottomTabNavigator,
 createStackNavigator,
 createAppContainer,
 createDrawerNavigator
} from 'react-navigation';
import MapScreen from './src/screen/MapScreen';
import FavoriteScreen from './src/screen/FavoriteScreen';
import ListScreen from './src/screen/ListScreen';
import FilterScreen from './src/screen/FilterScreen';
import PendingPayment from './src/screen/PendingPayment';
import PaymentHistory from './src/screen/PaymentHistory';
import TenantApplication from './src/screen/TenantApplication';
import MyProperties from './src/screen/MyProperties';
import MyCreditReport from './src/screen/MyCreditReport';
import MyTenants from './src/screen/MyTenants';
import ChangePassword from './src/screen/ChangePassword';
import SideBarDrawer from './src/screen/SideBarDrawer';
import Login from './src/screen/Login';
import PropertyInfo from './src/screen/PropertyInfo';
import LandlordPaymentHistory from './src/screen/LandlordPaymentHistory';
import InwardPendingPayment from './src/screen/InwardPendingPayment';
import OutwardPendingPayment from './src/screen/OutwardPendingPayment';
import ApplyForRent from './src/screen/ApplyForRent';
import Registration from './src/screen/Registration';
import ContactDetails from './src/screen/ContactDetails';
import MyProfile from './src/screen/MyProfile';
import Feedback from './src/screen/Feedback';
import AddCreditReport from './src/screen/AddCreditReport';
import AddNewProperty from './src/screen/AddNewProperty';

const { width } = Dimensions.get("window");
//export default createAppContainer(createDrawerNavigator(
//   const MyDrawerNavigator = createDrawerNavigator({
//   SettingsStack : {
//     screen: SettingsStack,
//   },
// //   {
// //   unmountInactiveRoutes:true,
// //   drawerPosition:'left',
// //   initialRouteName:'Map',
// //   contentComponent :SideBarDrawerSob,
// //  // drawerBackgroundColor:'#fff',
// //   drawerWidth:2*width/3
// // }
//   });

    
const App = createStackNavigator({
 //Constant which holds all the screens like index of any book
   MapScreen: { screen: MapScreen },
   //First entry by default be our first screen if we do not define initialRouteName
   ListScreen: { screen: ListScreen },
   FavoriteScreen: { screen: FavoriteScreen },
   FilterScreen: { screen: FilterScreen },
   
 },
 {
   headerMode: 'none',
 navigationOptions: {
   headerVisible: false,
 }
 }
);

const MyDrawerNavigator = createDrawerNavigator({

 Home_Menu_Label: {

   screen: App,

 },
  PendingPayment: {

   screen: PendingPayment,

 },
 PaymentHistory: {

   screen: PaymentHistory,

 },
  TenantApplication: {

   screen: TenantApplication,

 },
 MyProperties: {

   screen: MyProperties,

 },
  MyCreditReport: {

   screen: MyCreditReport,

 },
  MyTenants: {

   screen: MyTenants,

 },
  ChangePassword: {

   screen: ChangePassword,

 },
  SideBarDrawer: {

   screen: SideBarDrawer,

 },
 Login: {

   screen: Login,

 },
 PropertyInfo: {

   screen: PropertyInfo,

 },
  LandlordPaymentHistory: {

   screen: LandlordPaymentHistory,

 },
 InwardPendingPayment: {

   screen: InwardPendingPayment,

 },
 OutwardPendingPayment: {

   screen: OutwardPendingPayment,

 },
 ApplyForRent: {

   screen: ApplyForRent,

 },
  Registration: {

   screen: Registration,

 },
 ContactDetails: {

   screen: ContactDetails,
 },
 MyProfile: {

   screen: MyProfile,
 },
 Feedback: {

   screen: Feedback,
 },
 AddCreditReport: {

   screen: AddCreditReport,
 },
  AddNewProperty: {

   screen: AddNewProperty,
 },
},
{
 unmountInactiveRoutes:true,
 drawerPosition:'left',
// initialRouteName:'FilterScreen',
 contentComponent :SideBarDrawer,
// drawerBackgroundColor:'#fff',
 drawerWidth:2*width/3
}
);
 export default createAppContainer(MyDrawerNavigator);
//---------------------------------
// const SettingsStack = createStackNavigator({
//     Map: MapScreen,
//     Favorite: FavoriteScreen,
//     List : ListScreen
// },
// {
//   headerMode: 'none',
//   navigationOptions: {
//     headerVisible: false,
//   }
//  });

//   export default createAppContainer(createDrawerNavigator(

//   {
//     Map: SettingsStack,

//   },

// ));

///Documents/ReactDemo/Apartments/android/app